#include "periodic_callbacks.h"
#include "driver_can_bus_initializer.h"
#include "driver_logic.h"
#include "driver_msg_bank.h"
#include "lcd_2004.h"
#include "lcd_display2.h"
#include "sensor_led.h"

// This method is invoked once when the periodic tasks are created
void periodic_callbacks__initialize(void) {
  my_can_init(can1);
  sensor_led_init();
  I2C_init();
  set_LCD_to_4_bit_mode_init();
  LCD__init();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  // LCD_display_1Hz();
  // LCD_display_line1();
  // LCD_display_line2();
  // LCD_display_lines3n4();
}

// 100ms task
void periodic_callbacks__10Hz(uint32_t callback_count) {}

// 10ms task
void periodic_callbacks__100Hz(uint32_t callback_count) {

  // 50ms == 20Hz task
  if (callback_count % 5 == 0) {
    // Receive
    msg_bank__handle_msg_rx();
    msg_bank__service_mia_10hz();
    // Process
    driver__process_bridge_cmd();
    // driver__process_input();
    // driver__process_geo_controller_directions();
    // Transmit
    msg_bank__handle_msg_tx();
  }
}

void periodic_callbacks__1000Hz(uint32_t callback_count) {}
