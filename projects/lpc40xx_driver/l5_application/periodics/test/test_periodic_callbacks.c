#include <stdio.h>
#include <string.h>

#include "project.h"
#include "unity.h"

// Include the Mocks
#include "Mockcan_bus.h"
#include "Mockdriver_can_bus_initializer.h"
#include "Mockdriver_logic.h"
#include "Mockdriver_msg_bank.h"
#include "Mocklcd_2004.h"
#include "Mocklcd_display2.h"
#include "Mocksensor_led.h"

// Include the source we wish to test
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  my_can_init_Expect(can1);
  sensor_led_init_Expect();
  I2C_init_Expect();
  set_LCD_to_4_bit_mode_init_Expect();
  LCD__init_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) {
  LCD_display_1Hz_Expect();
  periodic_callbacks__1Hz(0);
}

void test__periodic_callbacks__20Hz(void) {
  msg_bank__handle_msg_rx_Expect();
  msg_bank__handle_msg_tx_Expect();
  msg_bank__service_mia_10hz_Expect();
  driver__process_bridge_cmd_Expect();
  // driver__process_input_Expect();
  // driver__process_geo_controller_directions_Expect();
  periodic_callbacks__100Hz(5);
}