#include "driver_logic.h"
#include "driver_msg_bank.h"

static dbc_APP_COMMAND_s app_cmd_received;
static dbc_SENSOR_DATA_s sensor_data_received;
static dbc_GEO_STATUS_s received_geo_status;
dbc_DRIVER_CONTROL_s motor_control_to_transmit = {};
dbc_DEBUG_DRIVER_s debug_data_to_transmit = {};

void driver__process_bridge_cmd(void) {
  app_cmd_received = msg_bank__get_app_command();
  app_cmd_received.BRIDGE_START_STOP = true; // delete this
  // printf("app cmd: %u\n", app_cmd_received.BRIDGE_START_STOP);

  if (app_cmd_received.BRIDGE_START_STOP == true) {
    driver__process_input();
    driver__process_geo_controller_directions();
    // printf("1\n");
  } else {
    motor_control_to_transmit = move_stop();
    // printf("0\n");
  }
}

void driver__process_input(void) {
  sensor_data_received = msg_bank__get_sensor_data();
  bool obstacle_avoidance_flag = is_obstacle_avoidance_required(sensor_data_received);

  // obstacle_avoidance_flag = false; // delete this
  if (obstacle_avoidance_flag) {
    motor_control_to_transmit = obstacle_avoidance();
    // printf("OA\n");
  } else {
    motor_control_to_transmit = waypoint_algo(received_geo_status);
    // printf("W\n");
  }

  printf("M: %d \t %d\n", motor_control_to_transmit.DRIVER_CONTROL_steer,
         motor_control_to_transmit.DRIVER_CONTROL_speed);

  debug_data_to_transmit.DRIVER_OA_or_W = obstacle_avoidance_flag;
  debug_data_to_transmit.DRIVER_OA_state = get_OA_state();
}

void driver__process_geo_controller_directions(void) {
  received_geo_status = msg_bank__get_geo_status();

  printf("B = %u\n", received_geo_status.GEO_STATUS_COMPASS_BEARING);
  printf("H = %u\n", received_geo_status.GEO_STATUS_COMPASS_HEADING);
  printf("d2d = %f\n", received_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION);
  // printf("GEO valid = %u\n", received_geo_status.GEO_STATUS_VALID_FLAG);
  // printf("GEO dest reached flag = %u\n", received_geo_status.GEO_STATUS_DESTINATION_REACHED);
}

dbc_DRIVER_CONTROL_s driver__get_motor_commands(void) { return motor_control_to_transmit; }

dbc_DEBUG_DRIVER_s driver__get_debug_info(void) { return debug_data_to_transmit; }
