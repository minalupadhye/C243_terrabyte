#pragma once

#include <stdio.h>

#include "obstacle_avoidance.h"
#include "project.h"
#include "waypoint_algorithm.h"

dbc_DRIVER_CONTROL_s driver__get_motor_commands(void);
dbc_DEBUG_DRIVER_s driver__get_debug_info(void);

void driver__process_bridge_cmd(void);
void driver__process_input(void);
void driver__process_geo_controller_directions(void);
