#include "driver_moves.h"

dbc_DRIVER_CONTROL_s moves_tx = {};

dbc_DRIVER_CONTROL_s move_hard_left(void) {
  moves_tx.DRIVER_CONTROL_steer = HARD_LEFT;
  moves_tx.DRIVER_CONTROL_speed = 1;
  return moves_tx;
}

dbc_DRIVER_CONTROL_s move_soft_left(void) {
  moves_tx.DRIVER_CONTROL_steer = SOFT_LEFT;
  moves_tx.DRIVER_CONTROL_speed = 1;
  return moves_tx;
}

dbc_DRIVER_CONTROL_s move_soft_right(void) {
  moves_tx.DRIVER_CONTROL_steer = SOFT_RIGHT;
  moves_tx.DRIVER_CONTROL_speed = 1;
  return moves_tx;
}

dbc_DRIVER_CONTROL_s move_hard_right(void) {
  moves_tx.DRIVER_CONTROL_steer = HARD_RIGHT;
  moves_tx.DRIVER_CONTROL_speed = 1;
  return moves_tx;
}

dbc_DRIVER_CONTROL_s move_forward_slow(void) {
  moves_tx.DRIVER_CONTROL_steer = NO_STEER;
  moves_tx.DRIVER_CONTROL_speed = 1;
  return moves_tx;
}

dbc_DRIVER_CONTROL_s move_forward_fast(void) {
  moves_tx.DRIVER_CONTROL_steer = NO_STEER;
  moves_tx.DRIVER_CONTROL_speed = 2;
  return moves_tx;
}

dbc_DRIVER_CONTROL_s move_stop(void) {
  moves_tx.DRIVER_CONTROL_steer = NO_STEER;
  moves_tx.DRIVER_CONTROL_speed = 0;
  return moves_tx;
}

dbc_DRIVER_CONTROL_s move_reverse(void) {
  moves_tx.DRIVER_CONTROL_steer = NO_STEER;
  moves_tx.DRIVER_CONTROL_speed = -1;
  return moves_tx;
}