#pragma once

#include "project.h"

typedef enum {
  HARD_LEFT = -2,
  SOFT_LEFT,
  NO_STEER,
  SOFT_RIGHT,
  HARD_RIGHT,
} steer__val_e;

dbc_DRIVER_CONTROL_s move_hard_left(void);
dbc_DRIVER_CONTROL_s move_soft_left(void);
dbc_DRIVER_CONTROL_s move_soft_right(void);
dbc_DRIVER_CONTROL_s move_hard_right(void);
dbc_DRIVER_CONTROL_s move_forward_slow(void);
dbc_DRIVER_CONTROL_s move_forward_fast(void);
dbc_DRIVER_CONTROL_s move_stop(void);
dbc_DRIVER_CONTROL_s move_reverse(void);
