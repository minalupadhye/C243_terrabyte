#include "driver_moves.h"
#include "project.h"
#include "unity.h"
#include "waypoint_algorithm.h"

// Include the Mocks
#include "Mockcan_bus.h"
#include "Mockdriver_msg_bank.h"
#include "Mockobstacle_avoidance.h"

// Include the source we wish to test
#include "driver_logic.h"

void setUp(void) {}

void tearDown(void) {}

void test__driver__process_input_OBSTACLE_AVOIDANCE(void) {
  dbc_SENSOR_DATA_s sensor_values = {};
  dbc_DRIVER_CONTROL_s motor_commands = {};
  sensor_values.SENSOR_DATA_left = 23;
  sensor_values.SENSOR_DATA_right = 55;
  sensor_values.SENSOR_DATA_front = 100;
  sensor_values.SENSOR_DATA_rear = 100;
  msg_bank__get_sensor_data_ExpectAndReturn(sensor_values);
  is_obstacle_avoidance_required_ExpectAndReturn(sensor_values, true);
  obstacle_avoidance_ExpectAndReturn(motor_commands);
  get_OA_state_ExpectAndReturn('F');
  driver__process_input();
}

void test__driver__process_input_GEO_DESTINATION(void) {
  dbc_SENSOR_DATA_s sensor_values = {};
  dbc_GEO_STATUS_s geo_status_input = {};
  dbc_DRIVER_CONTROL_s motor_commands = {};
  sensor_values.SENSOR_DATA_left = 200;
  sensor_values.SENSOR_DATA_right = 200;
  sensor_values.SENSOR_DATA_front = 200;
  sensor_values.SENSOR_DATA_rear = 200;
  msg_bank__get_sensor_data_ExpectAndReturn(sensor_values);
  is_obstacle_avoidance_required_ExpectAndReturn(sensor_values, false);
  geo_status_input.GEO_STATUS_VALID_FLAG = 1;
  geo_status_input.GEO_STATUS_COMPASS_BEARING = 90;
  geo_status_input.GEO_STATUS_COMPASS_HEADING = 10;
  geo_status_input.GEO_STATUS_DISTANCE_TO_DESTINATION = 2.5f;
  motor_commands = waypoint_algo(geo_status_input);
  TEST_ASSERT_EQUAL(1, motor_commands.DRIVER_CONTROL_speed);
  TEST_ASSERT_EQUAL(1, motor_commands.DRIVER_CONTROL_steer);
  get_OA_state_ExpectAndReturn('F');
  driver__process_input();
}
