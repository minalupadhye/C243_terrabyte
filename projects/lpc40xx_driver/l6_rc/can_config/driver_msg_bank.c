#include "driver_msg_bank.h"

#include <stdio.h>

// Include code modules that should receive their decoded CAN messages
#include "driver_logic.h"

static dbc_APP_COMMAND_s app_command = {};
static dbc_SENSOR_DATA_s sensor_data = {};
static dbc_GEO_STATUS_s geo_status = {};

void msg_bank__handle_msg_tx(void) {
  dbc_DRIVER_CONTROL_s tx_can_msg_motor_control = driver__get_motor_commands();
  dbc_DEBUG_DRIVER_s tx_driver_debug = driver__get_debug_info();

  // This function will put everything together and invoke dbc_send_can_message() internally
  // This avoids us having to re-construct CAN message data structure repetitively
  dbc_encode_and_send_DRIVER_CONTROL(NULL, &tx_can_msg_motor_control);
  dbc_encode_and_send_DEBUG_DRIVER(NULL, &tx_driver_debug);
}

void msg_bank__handle_msg_rx(void) {
  can__msg_t rx_can_msg = {};

  // Receive all messages
  while (can__rx(can1, &rx_can_msg, 0)) {
    const dbc_message_header_t header = {
        .message_id = rx_can_msg.msg_id,
        .message_dlc = rx_can_msg.frame_fields.data_len,
    };

    dbc_decode_APP_COMMAND(&app_command, header, rx_can_msg.data.bytes);
    dbc_decode_SENSOR_DATA(&sensor_data, header, rx_can_msg.data.bytes);
    dbc_decode_GEO_STATUS(&geo_status, header, rx_can_msg.data.bytes);
  }
}

void msg_bank__service_mia_10hz(void) {
  const uint32_t mia_increment_value_for_10Hz = 100; // change this to 50

  if (dbc_service_mia_APP_COMMAND(&app_command, mia_increment_value_for_10Hz)) {
    blink_all_leds();
    printf("APP COMMAND frame MIA.\n");
  }

  if (dbc_service_mia_SENSOR_DATA(&sensor_data, mia_increment_value_for_10Hz)) {
    blink_all_leds();
    printf("SENSOR frame MIA.\n");
  }

  if (dbc_service_mia_GEO_STATUS(&geo_status, mia_increment_value_for_10Hz)) {
    blink_all_leds();
    printf("GEO STATUS frame MIA.\n");
  }
}

dbc_APP_COMMAND_s msg_bank__get_app_command(void) { return app_command; }

dbc_SENSOR_DATA_s msg_bank__get_sensor_data(void) { return sensor_data; }

dbc_GEO_STATUS_s msg_bank__get_geo_status(void) { return geo_status; }
