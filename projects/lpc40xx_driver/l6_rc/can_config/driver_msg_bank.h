#pragma once

#include "can_bus.h"
#include "project.h"
#include "sj2_led_handle.h"

// Invoke this method in your periodic callbacks
void msg_bank__handle_msg_rx(void);
void msg_bank__handle_msg_tx(void);

// MIA management
void msg_bank__service_mia_10hz(void);

// "GET" APIs to obtain latest data
dbc_APP_COMMAND_s msg_bank__get_app_command(void);
dbc_SENSOR_DATA_s msg_bank__get_sensor_data(void);
dbc_GEO_STATUS_s msg_bank__get_geo_status(void);
