#include <stdio.h>

#include "dbc_to_can_driver_glue.h"
#include "mia_configurations.c"
#include "project.h"
#include "unity.h"

// Include the Mocks
#include "Mockcan_bus.h"
#include "Mockdriver_logic.h"
#include "Mocksj2_led_handle.h"

// Include the source we wish to test
#include "driver_msg_bank.h"

void setUp(void) {}

void tearDown(void) {}

void test__msg_bank__handle_msg_tx(void) {
  dbc_DRIVER_CONTROL_s motor_command_frame;
  driver__get_motor_commands_ExpectAndReturn(motor_command_frame);
  can__msg_t tx_can_msg1 = {};
  tx_can_msg1.msg_id = 300;
  tx_can_msg1.frame_fields.data_len = 2;
  tx_can_msg1.data.bytes[0] = 1;
  can__tx_ExpectAndReturn(can1, &tx_can_msg1, 0, true);
  can__tx_IgnoreArg_can_message_ptr();

  dbc_DEBUG_DRIVER_s driver_debug_frame;
  driver__get_debug_info_ExpectAndReturn(driver_debug_frame);
  can__msg_t tx_can_msg2 = {};
  tx_can_msg2.msg_id = 600;
  tx_can_msg2.frame_fields.data_len = 8;
  tx_can_msg2.data.bytes[0] = 1;
  can__tx_ExpectAndReturn(can1, &tx_can_msg2, 0, true);
  can__tx_IgnoreArg_can_message_ptr();

  msg_bank__handle_msg_tx();
}

void test__msg_bank__handle_msg_rx_SUCCESS(void) {
  can__msg_t test_rx_can_msg = {};

  test_rx_can_msg.msg_id = 200;
  test_rx_can_msg.frame_fields.data_len = 2;
  test_rx_can_msg.data.bytes[0] = 1;

  can__rx_ExpectAndReturn(can1, &test_rx_can_msg, 0, true);
  can__rx_IgnoreArg_can_message_ptr();
  can__rx_ReturnThruPtr_can_message_ptr(&test_rx_can_msg);

  can__rx_ExpectAndReturn(can1, NULL, 0, false);
  can__rx_IgnoreArg_can_message_ptr();

  msg_bank__handle_msg_rx();
}

void test__msg_bank__handle_msg_rx_UNKOWN_SENDER(void) {
  can__msg_t test_rx_can_msg = {};

  test_rx_can_msg.msg_id = 250;
  test_rx_can_msg.frame_fields.data_len = 1;
  test_rx_can_msg.data.bytes[0] = 5;

  can__rx_ExpectAndReturn(can1, &test_rx_can_msg, 0, true);
  can__rx_IgnoreArg_can_message_ptr();
  can__rx_ReturnThruPtr_can_message_ptr(&test_rx_can_msg);

  can__rx_ExpectAndReturn(can1, NULL, 0, false);
  can__rx_IgnoreArg_can_message_ptr();

  msg_bank__handle_msg_rx();
}

void test__msg_bank__service_mia_10hz_MIA_HAPPENED(void) {
  dbc_DRIVER_CONTROL_s test_sensor_data = {};
  test_sensor_data.mia_info.mia_counter = 400;
  // ADD HERE
  msg_bank__service_mia_10hz();
}

void test__msg_bank__service_mia_10hz_NO_MIA(void) {
  dbc_SENSOR_DATA_s test_sensor_data = {};
  test_sensor_data.mia_info.mia_counter = 0;
  // ADD HERE
  msg_bank__service_mia_10hz();
}

void test__msg_bank__get_sensor_data(void) {
  dbc_SENSOR_DATA_s test_sensor_data = {};
  dbc_SENSOR_DATA_s check_sensor_data = {};
  test_sensor_data.mia_info.mia_counter = 0;
  // ADD HERE
}