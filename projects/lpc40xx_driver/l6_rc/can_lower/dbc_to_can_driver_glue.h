#pragma once

#include "can_bus.h"
#include "project.h"

bool dbc_send_can_message(void *argument, uint32_t message_id, const uint8_t bytes[8], uint8_t dlc);
