#include "unity.h"

#include <stdio.h>

// Include the Mocks
#include "Mockcan_bus.h"

// Include the source we wish to test
#include "driver_can_bus_initializer.h"

void setUp(void) {}

void tearDown(void) {}

void test__my_can_init_for_can1(void) {
  can__init_ExpectAndReturn(can1, 500, 20, 20, NULL, NULL, true);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(can1);
  my_can_init(0);
}

void test__my_can_init_for_can2(void) {
  can__init_ExpectAndReturn(can2, 500, 20, 20, NULL, NULL, true);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(can2);
  my_can_init(1);
}

void test__my_can_init_for_can3(void) {
  can__init_ExpectAndReturn(2, 500, 20, 20, NULL, NULL, false);
  my_can_init(2);
}

void test__my_can_init_for_can256(void) {
  can__init_ExpectAndReturn(255, 500, 20, 20, NULL, NULL, false);
  my_can_init(255);
}
