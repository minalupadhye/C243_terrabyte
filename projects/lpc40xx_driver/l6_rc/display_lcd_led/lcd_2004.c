#include "lcd_2004.h"

// static const uint8_t ACTUAL_LCD_ADDRESS = 0x27;
static const uint8_t LCD_SLAVE_ADDRESS = 0x4E;
static const i2c_e LCD_I2C_bus = I2C__2;
static gpio_s switch_01;

void I2C_init(void) {
  gpio__construct_with_function(GPIO__PORT_0, 10, GPIO__FUNCTION_2);
  gpio__construct_with_function(GPIO__PORT_0, 11, GPIO__FUNCTION_2);

  if (i2c__detect(I2C__2, LCD_SLAVE_ADDRESS)) {
    // printf("I2C init.\n");
  }
}

void set_LCD_to_4_bit_mode_init(void) {
  uint8_t LCD_REGISTER = 0x00;
  delay__ms(50);
  i2c__write_single(LCD_I2C_bus, LCD_SLAVE_ADDRESS, LCD_REGISTER, 0x38);
  delay__ms(10);
  i2c__write_single(LCD_I2C_bus, LCD_SLAVE_ADDRESS, LCD_REGISTER, 0x38);
  delay__ms(10);
  i2c__write_single(LCD_I2C_bus, LCD_SLAVE_ADDRESS, LCD_REGISTER, 0x38);
  delay__ms(10);
  i2c__write_single(LCD_I2C_bus, LCD_SLAVE_ADDRESS, LCD_REGISTER, 0x28);
  delay__ms(10);
  // printf("4 bit mode\n");
}

void LCD__init(void) {
  send_LCD_command(0x28); // Function set
  delay__ms(2);
  send_LCD_command(0x0C); // Display on
  delay__ms(1);
  send_LCD_command(0x01); // Clear screen
  delay__ms(1);
  send_LCD_command(0x06); // Turn on auto-increment
  delay__ms(2);
  send_LCD_command(0x80); // Start of LCD
  delay__ms(2);
  // printf("LCD init\n");
}

void send_LCD_command(uint8_t command) {
  uint8_t byte;
  uint8_t LCD_REGISTER = 0x00;
  /*Start with the upper nibble*/
  byte = (command & 0xF0);
  byte |= 0x08; // backlight of LCD
  byte |= 0x04; // Enable bit LCD
  i2c__write_single(LCD_I2C_bus, LCD_SLAVE_ADDRESS, LCD_REGISTER, byte);
  byte &= ~0x04;
  byte |= 0x08;
  i2c__write_single(LCD_I2C_bus, LCD_SLAVE_ADDRESS, LCD_REGISTER, byte);
  /*lower nibble*/
  byte = (command & 0x0F) << 4;
  byte |= 0x08;
  byte |= 0x04;
  i2c__write_single(LCD_I2C_bus, LCD_SLAVE_ADDRESS, LCD_REGISTER, byte);
  byte &= ~0x04;
  byte |= 0x08;
  i2c__write_single(LCD_I2C_bus, LCD_SLAVE_ADDRESS, LCD_REGISTER, byte);
  delay__ms(3);
}

void send_LCD_char(uint8_t character) {
  uint8_t byte;
  uint8_t LCD_REGISTER = 0x00;
  /*Start with the upper nibble*/
  byte = (character & 0xF0);
  byte |= 0x08;
  byte |= (0x04 | 0x01);
  i2c__write_single(LCD_I2C_bus, LCD_SLAVE_ADDRESS, LCD_REGISTER, byte);
  byte &= ~(0x04 | 0x01);
  byte |= 0x08;
  i2c__write_single(LCD_I2C_bus, LCD_SLAVE_ADDRESS, LCD_REGISTER, byte);
  delay__ms(2);
  /*Now do the lower nibble*/
  byte = (character & 0x0F) << 4;
  byte |= 0x08;
  byte |= (0x04 | 0x01);
  i2c__write_single(LCD_I2C_bus, LCD_SLAVE_ADDRESS, LCD_REGISTER, byte);
  byte &= ~(0x04 | 0x01);
  byte |= 0x08;
  i2c__write_single(LCD_I2C_bus, LCD_SLAVE_ADDRESS, LCD_REGISTER, byte);
  delay__ms(2);
}

void send_LCD_string(char *input_string) {
  uint8_t i = 0;
  while (i != strnlen(input_string, 20)) {
    send_LCD_char(input_string[i]);
    i++;
  }
}

// void switch_init(void) { switch_01 = gpio__construct_as_input(GPIO__PORT_1, 19); }

// void LCD_reset_by_switch(void) {
//   if (gpio__get(switch_01)) {
//     send_LCD_command(0x01);
//   }
// }
