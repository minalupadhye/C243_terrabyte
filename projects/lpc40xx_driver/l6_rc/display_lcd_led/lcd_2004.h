#pragma once

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "board_io.h"
#include "delay.h"
#include "gpio.h"
#include "i2c.h"

void I2C_init(void);
void set_LCD_to_4_bit_mode_init(void);
void LCD__init(void);
void switch_init(void);

void send_LCD_command(uint8_t command);
void send_LCD_char(uint8_t character);
void send_LCD_string(char *input_string);
void LCD_reset_by_switch(void);
