#include "lcd_display2.h"

#define LCD_LINE1 0x80
#define LCD_LINE2 0xC0
#define LCD_LINE3 0x94
#define LCD_LINE4 0xD4

void LCD_display_1Hz(void) {
  // char lcd_buffer[30] = {};
  // send_LCD_command(0x01); // Clear Screen before update
  // delay__ms(2);

  // dbc_SENSOR_DATA_s sensor_values = msg_bank__get_sensor_data();
  // dbc_GEO_STATUS_s geo_values = msg_bank__get_geo_status();
  // dbc_DRIVER_CONTROL_s driver_cmd_values = driver__get_motor_commands();
  // uint8_t left = sensor_values.SENSOR_DATA_left;
  // uint8_t right = sensor_values.SENSOR_DATA_right;
  // uint8_t front = sensor_values.SENSOR_DATA_front;
  // uint8_t rear = sensor_values.SENSOR_DATA_rear;

  // int32_t distance_to_destination = floor(geo_values.GEO_STATUS_DISTANCE_TO_DESTINATION);

  // send_LCD_command(LCD_LINE1);
  // sprintf(lcd_buffer, "BLFR:%u,%u,%u,%u", rear, left, front, right);
  // send_LCD_string(lcd_buffer);
  // delay__ms(1);

  // send_LCD_command(LCD_LINE2);
  // sprintf(lcd_buffer, "SPEED:%d; STEER:%d", driver_cmd_values.DRIVER_CONTROL_speed,
  //         driver_cmd_values.DRIVER_CONTROL_steer);
  // send_LCD_string(lcd_buffer);
  // delay__ms(1);

  // send_LCD_command(LCD_LINE3);
  // sprintf(lcd_buffer, "D2D:%ld; V:%d", distance_to_destination, geo_values.GEO_STATUS_VALID_FLAG);
  // send_LCD_string(lcd_buffer);
  // delay__ms(1);

  // send_LCD_command(LCD_LINE4);
  // sprintf(lcd_buffer, "H:%d; B:%d", geo_values.GEO_STATUS_COMPASS_HEADING, geo_values.GEO_STATUS_COMPASS_BEARING);
  // send_LCD_string(lcd_buffer);
  // delay__ms(1);
}

void LCD_display_line1(void) {
  char lcd_buffer[30] = {};
  send_LCD_command(0x01); // Clear Screen before update
  delay__ms(2);

  // dbc_SENSOR_DATA_s sensor_values = msg_bank__get_sensor_data();
  send_LCD_command(LCD_LINE1);
  sprintf(lcd_buffer, "BLFR:144 234 222 111");
  // sprintf(lcd_buffer, "BLFR:%u,%u,%u,%u", sensor_values.SENSOR_DATA_rear, sensor_values.SENSOR_DATA_left,
  //         sensor_values.SENSOR_DATA_front, sensor_values.SENSOR_DATA_right);
  send_LCD_string(lcd_buffer);
  // delay__ms(1);
}

void LCD_display_line2(void) {
  char lcd_buffer[30] = {};

  dbc_DRIVER_CONTROL_s driver_cmd_values = driver__get_motor_commands();
  send_LCD_command(LCD_LINE2);
  sprintf(lcd_buffer, "SPEED:%d; STEER:%d", driver_cmd_values.DRIVER_CONTROL_speed,
          driver_cmd_values.DRIVER_CONTROL_steer);
  send_LCD_string(lcd_buffer);
  delay__ms(1);
}

void LCD_display_lines3n4(void) {
  char lcd_buffer[30] = {};
  dbc_GEO_STATUS_s geo_values = msg_bank__get_geo_status();
  int32_t distance_to_destination = floor(geo_values.GEO_STATUS_DISTANCE_TO_DESTINATION);

  send_LCD_command(LCD_LINE3);
  sprintf(lcd_buffer, "D2D:%ld; V:%d", distance_to_destination, geo_values.GEO_STATUS_VALID_FLAG);
  send_LCD_string(lcd_buffer);
  delay__ms(1);

  send_LCD_command(LCD_LINE4);
  sprintf(lcd_buffer, "H:%d; B:%d", geo_values.GEO_STATUS_COMPASS_HEADING, geo_values.GEO_STATUS_COMPASS_BEARING);
  send_LCD_string(lcd_buffer);
  delay__ms(1);
}