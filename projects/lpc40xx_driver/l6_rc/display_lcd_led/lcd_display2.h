#pragma once

#include <math.h>
#include <stdio.h>
#include <string.h>

#include "delay.h"
#include "driver_logic.h"
#include "driver_msg_bank.h"
#include "lcd_2004.h"
#include "project.h"

void LCD_display_1Hz(void);
void LCD_display_line1(void);
void LCD_display_line2(void);
void LCD_display_lines3n4(void);
