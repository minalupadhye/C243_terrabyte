#include "sensor_led.h"

static gpio_s led_front, led_rear, led_left, led_right;

void sensor_led_init(void) {
  led_front = gpio__construct_as_output(GPIO__PORT_2, 0);
  led_rear = gpio__construct_as_output(GPIO__PORT_2, 1);
  led_left = gpio__construct_as_output(GPIO__PORT_2, 2);
  led_right = gpio__construct_as_output(GPIO__PORT_2, 4);
}

void set_led_front(void) { gpio__set(led_front); }

void reset_led_front(void) { gpio__reset(led_front); }

void set_led_rear(void) { gpio__set(led_rear); }

void reset_led_rear(void) { gpio__reset(led_rear); }

void set_led_left(void) { gpio__set(led_left); }

void reset_led_left(void) { gpio__reset(led_left); }

void set_led_right(void) { gpio__set(led_right); }

void reset_led_right(void) { gpio__reset(led_right); }
