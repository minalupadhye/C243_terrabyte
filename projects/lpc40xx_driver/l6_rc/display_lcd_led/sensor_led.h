#pragma once

#include "gpio.h"

void sensor_led_init(void);
void set_led_front(void);
void reset_led_front(void);
void set_led_rear(void);
void reset_led_rear(void);
void set_led_left(void);
void reset_led_left(void);
void set_led_right(void);
void reset_led_right(void);
