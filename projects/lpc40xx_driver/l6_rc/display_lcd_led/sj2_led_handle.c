#include "sj2_led_handle.h"

const uint8_t led_delay = 3;

void blink_all_leds(void) {
  gpio__reset(board_io__get_led0());
  gpio__reset(board_io__get_led1());
  gpio__reset(board_io__get_led2());
  gpio__reset(board_io__get_led3());
  // vTaskDelay(led_delay);
  // gpio__set(board_io__get_led0());
  // gpio__set(board_io__get_led1());
  // gpio__set(board_io__get_led2());
  // gpio__set(board_io__get_led3());
  // vTaskDelay(led_delay);
}

void blink_led_0(void) {
  gpio__reset(board_io__get_led0());
  vTaskDelay(led_delay);
  gpio__set(board_io__get_led0());
  vTaskDelay(led_delay);
}

void blink_led_1(void) {
  gpio__reset(board_io__get_led1());
  vTaskDelay(led_delay);
  gpio__set(board_io__get_led1());
  vTaskDelay(led_delay);
}

void blink_led_2(void) {
  gpio__reset(board_io__get_led2());
  vTaskDelay(led_delay);
  gpio__set(board_io__get_led2());
  vTaskDelay(led_delay);
}

void blink_led_3(void) {
  gpio__reset(board_io__get_led3());
  vTaskDelay(led_delay);
  gpio__set(board_io__get_led3());
  vTaskDelay(led_delay);
}
