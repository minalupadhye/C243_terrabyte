#pragma once

#include "FreeRTOS.h"
#include "board_io.h"
#include "gpio.h"
#include "task.h"

void blink_all_leds(void);
void blink_led_0(void);
void blink_led_1(void);
void blink_led_2(void);
void blink_led_3(void);
