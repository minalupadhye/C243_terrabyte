#include "obstacle_avoidance.h"
#include "sensor_led.h"

const uint8_t FRONT_STOP_THRESHOLD = 70;
const uint8_t SIDE_SOFT_THRESHOLD = 70;
const uint8_t SIDE_HARD_THRESHOLD = 50;
const uint8_t REAR_THRESHOLD = 40;

static uint8_t left_sensor_cm;
static uint8_t right_sensor_cm;
static uint8_t front_sensor_cm;
static uint8_t rear_sensor_cm;

dbc_DRIVER_CONTROL_s sensor_based_motor_command = {};
static char OA_state = 'S';

static void sensor_led_monitor(void) {
  if (front_sensor_cm < FRONT_STOP_THRESHOLD) {
    set_led_front();
  } else {
    reset_led_front();
  }

  if (rear_sensor_cm < REAR_THRESHOLD) {
    set_led_rear();
  } else {
    reset_led_rear();
  }

  if (left_sensor_cm < SIDE_SOFT_THRESHOLD) {
    set_led_left();
  } else {
    reset_led_left();
  }

  if (right_sensor_cm < SIDE_SOFT_THRESHOLD) {
    set_led_right();
  } else {
    reset_led_right();
  }
}

static void process_left_sensor(void) {
  if (left_sensor_cm < SIDE_HARD_THRESHOLD) {
    sensor_based_motor_command = move_hard_right();
  } else if ((left_sensor_cm >= SIDE_HARD_THRESHOLD) && (left_sensor_cm < SIDE_SOFT_THRESHOLD)) {
    sensor_based_motor_command = move_soft_right();
  }
}

static void process_right_sensor(void) {
  if (right_sensor_cm < SIDE_HARD_THRESHOLD) {
    sensor_based_motor_command = move_hard_left();
  } else if ((right_sensor_cm >= SIDE_HARD_THRESHOLD) && (right_sensor_cm < SIDE_SOFT_THRESHOLD)) {
    sensor_based_motor_command = move_soft_left();
  }
}

static void stop_state(void) {
  sensor_based_motor_command = move_stop();
  if (front_sensor_cm < FRONT_STOP_THRESHOLD) {
    OA_state = 'R';
  } else {
    OA_state = 'F';
  }
}

static void reverse_state(void) {
  if (front_sensor_cm < FRONT_STOP_THRESHOLD) {
    sensor_based_motor_command = move_reverse();
  } else {
    OA_state = 'S';
  }
}

static void forward_state(void) {
  if (front_sensor_cm < FRONT_STOP_THRESHOLD) {
    OA_state = 'S';
  } else if ((left_sensor_cm < SIDE_SOFT_THRESHOLD) || right_sensor_cm < SIDE_SOFT_THRESHOLD) {
    if (left_sensor_cm < right_sensor_cm) {
      process_left_sensor();
    } else {
      process_right_sensor();
    }
  } else {
    sensor_based_motor_command = move_forward_slow();
  }
}

char get_OA_state(void) { return OA_state; }

bool is_obstacle_avoidance_required(dbc_SENSOR_DATA_s sensor_data_rx) {
  bool steering_flag = 0;

  left_sensor_cm = sensor_data_rx.SENSOR_DATA_left;
  right_sensor_cm = sensor_data_rx.SENSOR_DATA_right;
  front_sensor_cm = sensor_data_rx.SENSOR_DATA_front;
  rear_sensor_cm = sensor_data_rx.SENSOR_DATA_rear;

  // printf("left sensor = %u\n", left_sensor_cm);
  // printf("right sensor = %u\n", right_sensor_cm);
  // printf("front sensor = %u\n", front_sensor_cm);
  // printf("rear sensor = %u\n", rear_sensor_cm);

  sensor_led_monitor();

  if (right_sensor_cm < SIDE_SOFT_THRESHOLD || left_sensor_cm < SIDE_SOFT_THRESHOLD ||
      front_sensor_cm < FRONT_STOP_THRESHOLD) {
    steering_flag = 1;
  }
  return steering_flag;
}

dbc_DRIVER_CONTROL_s obstacle_avoidance(void) {
  // printf("%c\n", OA_state);

  switch (OA_state) {
  case 'S':
    stop_state();
    break;

  case 'R':
    reverse_state();
    break;

  case 'F':
    forward_state();
    break;

  default:
    OA_state = 'S';
  }

  return sensor_based_motor_command;
}
