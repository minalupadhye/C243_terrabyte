#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "driver_moves.h"
#include "project.h"

bool is_obstacle_avoidance_required(dbc_SENSOR_DATA_s sensor_data_rx);
dbc_DRIVER_CONTROL_s obstacle_avoidance(void);
char get_OA_state(void);
