#include "driver_moves.h"
#include "unity.h"

// Include the Mocks
#include "Mocksensor_led.h"

// Include the source we wish to test
#include "obstacle_avoidance.c"

void setUp(void) {}

void tearDown(void) {}

void test__is_obstacle_avoidance_required(void) {
  dbc_SENSOR_DATA_s sensor_values = {};
  sensor_values.SENSOR_DATA_front = FRONT_STOP_THRESHOLD;
  sensor_values.SENSOR_DATA_rear = FRONT_STOP_THRESHOLD;
  sensor_values.SENSOR_DATA_left = SIDE_SOFT_THRESHOLD - 1;
  sensor_values.SENSOR_DATA_right = SIDE_SOFT_THRESHOLD;
  reset_led_front_Expect();
  reset_led_rear_Expect();
  set_led_left_Expect();
  reset_led_right_Expect();
  TEST_ASSERT_TRUE(is_obstacle_avoidance_required(sensor_values));
  sensor_values.SENSOR_DATA_left = SIDE_SOFT_THRESHOLD;
  sensor_values.SENSOR_DATA_right = SIDE_SOFT_THRESHOLD - 1;
  reset_led_front_Expect();
  reset_led_rear_Expect();
  reset_led_left_Expect();
  set_led_right_Expect();
  TEST_ASSERT_TRUE(is_obstacle_avoidance_required(sensor_values));
  sensor_values.SENSOR_DATA_left = 15;
  sensor_values.SENSOR_DATA_right = SIDE_HARD_THRESHOLD;
  reset_led_front_Expect();
  reset_led_rear_Expect();
  set_led_left_Expect();
  set_led_right_Expect();
  TEST_ASSERT_TRUE(is_obstacle_avoidance_required(sensor_values));
  sensor_values.SENSOR_DATA_left = SIDE_HARD_THRESHOLD;
  sensor_values.SENSOR_DATA_right = 20;
  reset_led_front_Expect();
  reset_led_rear_Expect();
  set_led_left_Expect();
  set_led_right_Expect();
  TEST_ASSERT_TRUE(is_obstacle_avoidance_required(sensor_values));
}

void test__is_obstacle_avoidance_required_2(void) {
  dbc_SENSOR_DATA_s sensor_values = {};
  sensor_values.SENSOR_DATA_front = FRONT_STOP_THRESHOLD;
  sensor_values.SENSOR_DATA_rear = FRONT_STOP_THRESHOLD;
  sensor_values.SENSOR_DATA_left = SIDE_SOFT_THRESHOLD;
  sensor_values.SENSOR_DATA_right = SIDE_SOFT_THRESHOLD;
  reset_led_front_Expect();
  reset_led_rear_Expect();
  reset_led_left_Expect();
  reset_led_right_Expect();
  TEST_ASSERT_FALSE(is_obstacle_avoidance_required(sensor_values));
  sensor_values.SENSOR_DATA_left = SIDE_SOFT_THRESHOLD;
  sensor_values.SENSOR_DATA_right = SIDE_SOFT_THRESHOLD + 10;
  reset_led_front_Expect();
  reset_led_rear_Expect();
  reset_led_left_Expect();
  reset_led_right_Expect();
  TEST_ASSERT_FALSE(is_obstacle_avoidance_required(sensor_values));
  sensor_values.SENSOR_DATA_left = SIDE_SOFT_THRESHOLD + 15;
  sensor_values.SENSOR_DATA_right = SIDE_SOFT_THRESHOLD;
  reset_led_front_Expect();
  reset_led_rear_Expect();
  reset_led_left_Expect();
  reset_led_right_Expect();
  TEST_ASSERT_FALSE(is_obstacle_avoidance_required(sensor_values));
  sensor_values.SENSOR_DATA_left = SIDE_SOFT_THRESHOLD + 20;
  sensor_values.SENSOR_DATA_right = SIDE_SOFT_THRESHOLD + 20;
  reset_led_front_Expect();
  reset_led_rear_Expect();
  reset_led_left_Expect();
  reset_led_right_Expect();
  TEST_ASSERT_FALSE(is_obstacle_avoidance_required(sensor_values));
}

void test__obstacle_avoidance_NO_STEER_HIGHER_RIGHT(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD + 10;
  rear_sensor_cm = FRONT_STOP_THRESHOLD + 25;
  left_sensor_cm = SIDE_SOFT_THRESHOLD + 10;
  right_sensor_cm = SIDE_SOFT_THRESHOLD + 20;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance(); // OA not in action
  TEST_ASSERT_EQUAL(NO_STEER, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_NO_STEER_HIGHER_LEFT(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD + 30;
  rear_sensor_cm = FRONT_STOP_THRESHOLD + 20;
  left_sensor_cm = SIDE_SOFT_THRESHOLD + 30;
  right_sensor_cm = SIDE_SOFT_THRESHOLD + 15;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance(); // OA not in action
  TEST_ASSERT_EQUAL(NO_STEER, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_NO_STEER_INITIAL_THRESHOLD(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_SOFT_THRESHOLD;
  right_sensor_cm = SIDE_SOFT_THRESHOLD;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance(); // OA not in action
  TEST_ASSERT_EQUAL(NO_STEER, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_SOFT_RIGHT_EASY(void) {
  dbc_DRIVER_CONTROL_s motor_cmd;
  front_sensor_cm = FRONT_STOP_THRESHOLD;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_SOFT_THRESHOLD - 1;
  right_sensor_cm = SIDE_SOFT_THRESHOLD;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(SOFT_RIGHT, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_SOFT_LEFT_EASY(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_SOFT_THRESHOLD;
  right_sensor_cm = SIDE_SOFT_THRESHOLD - 1;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(SOFT_LEFT, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_SOFT_RIGHT_TRICKY(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_HARD_THRESHOLD + 5;
  right_sensor_cm = SIDE_SOFT_THRESHOLD - 5;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(SOFT_RIGHT, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_SOFT_LEFT_TRICKY(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_SOFT_THRESHOLD - 5;
  right_sensor_cm = SIDE_HARD_THRESHOLD + 5;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(SOFT_LEFT, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_HARD_RIGHT_EASY(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_HARD_THRESHOLD - 10;
  right_sensor_cm = SIDE_SOFT_THRESHOLD + 20;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(HARD_RIGHT, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_HARD_LEFT_EASY(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_SOFT_THRESHOLD + 10;
  right_sensor_cm = SIDE_HARD_THRESHOLD - 10;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(HARD_LEFT, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_HARD_RIGHT_TRICKY(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_HARD_THRESHOLD - 5;
  right_sensor_cm = SIDE_HARD_THRESHOLD + 10;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(HARD_RIGHT, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_HARD_LEFT_TRICKY(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_HARD_THRESHOLD + 10;
  right_sensor_cm = SIDE_HARD_THRESHOLD - 5;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(HARD_LEFT, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_MAX_CONFUSION_HIGHER_RIGHT(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_HARD_THRESHOLD - 1;
  right_sensor_cm = SIDE_HARD_THRESHOLD - 5;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(HARD_LEFT, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_MAX_CONFUSION_HIGHER_LEFT(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_HARD_THRESHOLD - 7;
  right_sensor_cm = SIDE_HARD_THRESHOLD - 1;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(HARD_RIGHT, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_EQUAL_INITIAL_THRESHOLD(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_SOFT_THRESHOLD - 1;
  right_sensor_cm = SIDE_SOFT_THRESHOLD - 1;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(SOFT_LEFT, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_EQUAL_CRITICAL_THREHOLD(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_HARD_THRESHOLD - 1;
  right_sensor_cm = SIDE_HARD_THRESHOLD - 1;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(HARD_LEFT, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_STATE_TRANSITION_F_to_S(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD - 5;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_SOFT_THRESHOLD;
  right_sensor_cm = SIDE_SOFT_THRESHOLD;

  OA_state = 'F';
  motor_cmd = obstacle_avoidance();
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(NO_STEER, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(0, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_STATE_TRANSITION_S_to_R(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD - 5;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_SOFT_THRESHOLD;
  right_sensor_cm = SIDE_SOFT_THRESHOLD;

  OA_state = 'S';
  motor_cmd = obstacle_avoidance();
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(NO_STEER, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(-1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_STATE_TRANSITION_R_to_S(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_SOFT_THRESHOLD;
  right_sensor_cm = SIDE_SOFT_THRESHOLD;

  OA_state = 'R';
  motor_cmd = obstacle_avoidance();
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(NO_STEER, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(0, motor_cmd.DRIVER_CONTROL_speed);
}

void test__obstacle_avoidance_STATE_TRANSITION_S_to_F(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  front_sensor_cm = FRONT_STOP_THRESHOLD + 5;
  rear_sensor_cm = FRONT_STOP_THRESHOLD;
  left_sensor_cm = SIDE_SOFT_THRESHOLD;
  right_sensor_cm = SIDE_SOFT_THRESHOLD;

  OA_state = 'S';
  motor_cmd = obstacle_avoidance();
  motor_cmd = obstacle_avoidance();
  TEST_ASSERT_EQUAL(NO_STEER, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}