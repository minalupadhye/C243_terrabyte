#include "driver_moves.h"
#include "project.h"
#include "unity.h"

// Include the Mocks
// #include "Mockcan_bus.h"

// Include the source we wish to test
#include "waypoint_algorithm.c"

void setUp(void) {}

void tearDown(void) {}

void test__waypoint_algo__DESTINATION_REACHED(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  dbc_GEO_STATUS_s geo_status;
  geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = 0.2f;
  motor_cmd = waypoint_algo(geo_status);
  TEST_ASSERT_EQUAL(0, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(0, motor_cmd.DRIVER_CONTROL_speed);
}

void test__waypoint_algo__SIMILAR_HEADING_BEARING(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  dbc_GEO_STATUS_s geo_status;
  geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = DESTINATION_RANGE;
  geo_status.GEO_STATUS_COMPASS_BEARING = 35;
  geo_status.GEO_STATUS_COMPASS_HEADING = 37;
  geo_status.GEO_STATUS_VALID_FLAG = 1;
  motor_cmd = waypoint_algo(geo_status);
  TEST_ASSERT_EQUAL(0, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(2, motor_cmd.DRIVER_CONTROL_speed);
}

void test__waypoint_algo__TURN_RIGHT(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  dbc_GEO_STATUS_s geo_status;
  geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = DESTINATION_RANGE;
  geo_status.GEO_STATUS_COMPASS_BEARING = 60;
  geo_status.GEO_STATUS_COMPASS_HEADING = 35;
  geo_status.GEO_STATUS_VALID_FLAG = 1;
  motor_cmd = waypoint_algo(geo_status);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__waypoint_algo__TURN_LEFT(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  dbc_GEO_STATUS_s geo_status;
  geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = DESTINATION_RANGE;
  geo_status.GEO_STATUS_COMPASS_BEARING = 40;
  geo_status.GEO_STATUS_COMPASS_HEADING = 59;
  geo_status.GEO_STATUS_VALID_FLAG = 1;
  motor_cmd = waypoint_algo(geo_status);
  TEST_ASSERT_EQUAL(-1, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__waypoint_algo__TRICKY_LEFT(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  dbc_GEO_STATUS_s geo_status;
  geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = DESTINATION_RANGE;
  geo_status.GEO_STATUS_COMPASS_BEARING = 350;
  geo_status.GEO_STATUS_COMPASS_HEADING = 40;
  geo_status.GEO_STATUS_VALID_FLAG = 1;
  motor_cmd = waypoint_algo(geo_status);
  TEST_ASSERT_EQUAL(-1, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__waypoint_algo__TRICKY_RIGHT(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  dbc_GEO_STATUS_s geo_status;
  geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = DESTINATION_RANGE;
  geo_status.GEO_STATUS_COMPASS_BEARING = 15;
  geo_status.GEO_STATUS_COMPASS_HEADING = 340;
  geo_status.GEO_STATUS_VALID_FLAG = 1;
  motor_cmd = waypoint_algo(geo_status);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__waypoint_algo__ANGLE_LIMIT_1(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  dbc_GEO_STATUS_s geo_status;
  geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = DESTINATION_RANGE;
  geo_status.GEO_STATUS_COMPASS_BEARING = 0;
  geo_status.GEO_STATUS_COMPASS_HEADING = 180;
  geo_status.GEO_STATUS_VALID_FLAG = 1;
  motor_cmd = waypoint_algo(geo_status);
  TEST_ASSERT_EQUAL(-1, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__waypoint_algo__ANGLE_LIMIT_2(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  dbc_GEO_STATUS_s geo_status;
  geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = DESTINATION_RANGE;
  geo_status.GEO_STATUS_COMPASS_BEARING = 0;
  geo_status.GEO_STATUS_COMPASS_HEADING = 181;
  geo_status.GEO_STATUS_VALID_FLAG = 1;
  motor_cmd = waypoint_algo(geo_status);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__waypoint_algo__ANGLE_LIMIT_3(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  dbc_GEO_STATUS_s geo_status;
  geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = DESTINATION_RANGE;
  geo_status.GEO_STATUS_COMPASS_BEARING = 180;
  geo_status.GEO_STATUS_COMPASS_HEADING = 0;
  geo_status.GEO_STATUS_VALID_FLAG = 1;
  motor_cmd = waypoint_algo(geo_status);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}

void test__waypoint_algo__ANGLE_LIMIT_4(void) {
  dbc_DRIVER_CONTROL_s motor_cmd = {};
  dbc_GEO_STATUS_s geo_status;
  geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION = DESTINATION_RANGE;
  geo_status.GEO_STATUS_COMPASS_BEARING = 181;
  geo_status.GEO_STATUS_COMPASS_HEADING = 0;
  geo_status.GEO_STATUS_VALID_FLAG = 1;
  motor_cmd = waypoint_algo(geo_status);
  TEST_ASSERT_EQUAL(-1, motor_cmd.DRIVER_CONTROL_steer);
  TEST_ASSERT_EQUAL(1, motor_cmd.DRIVER_CONTROL_speed);
}
