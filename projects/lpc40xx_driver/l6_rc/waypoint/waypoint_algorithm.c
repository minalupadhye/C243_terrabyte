#include "waypoint_algorithm.h"

// const float DESTINATION_RANGE = 0.5f;

static int16_t calculate_angle(uint16_t heading, uint16_t bearing) {
  int16_t angle;
  angle = bearing - heading;

  if (angle > 180) {
    angle = angle - 360;
  } else if (angle < -180) {
    angle = angle + 360;
  }

  // printf("H-B is %d.\n", angle);
  printf("%d\n", angle);

  return angle;
}

static bool turn_towards_destination(int16_t signed_angle) {
  bool turn_flag = 0;

  if (signed_angle > 0) {
    turn_flag = 1;
  }

  return turn_flag;
}

dbc_DRIVER_CONTROL_s waypoint_algo(dbc_GEO_STATUS_s geo_status_rx) {
  dbc_DRIVER_CONTROL_s geo_based_motor_command = {};
  int16_t correction_angle;
  bool left_or_right, waypoint_enable;

  waypoint_enable =
      (geo_status_rx.GEO_STATUS_VALID_FLAG == true) && (geo_status_rx.GEO_STATUS_DESTINATION_REACHED == false);

  // waypoint_enable = (geo_status_rx.GEO_STATUS_DISTANCE_TO_DESTINATION >= DESTINATION_RANGE) &&
  //                   (geo_status_rx.GEO_STATUS_VALID_FLAG == true);

  if (waypoint_enable) {
    correction_angle =
        calculate_angle(geo_status_rx.GEO_STATUS_COMPASS_HEADING, geo_status_rx.GEO_STATUS_COMPASS_BEARING);

    if (correction_angle < -2 || correction_angle > 2) {
      left_or_right = turn_towards_destination(correction_angle);

      if (left_or_right) {
        geo_based_motor_command = move_soft_right();
      } else {
        geo_based_motor_command = move_soft_left();
      }

    } else {
      geo_based_motor_command = move_forward_fast();
    }

  } else {
    geo_based_motor_command = move_stop();
  }

  return geo_based_motor_command;
}
