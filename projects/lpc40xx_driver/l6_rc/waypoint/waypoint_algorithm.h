#pragma once

#include <stdio.h>

#include "driver_moves.h"
#include "project.h"

dbc_DRIVER_CONTROL_s waypoint_algo(dbc_GEO_STATUS_s geo_status_rx);
