

/*

SJ2 Board Pins mapped to RC car
+------------------------------------+

            -------------+                       +   -------------
            -------------+                       +   Vcc:   3.3V
            -------------+                       +   -------------
GPS RX      P4.28: TX3-  +                       +   RX3:   P4.29   GPS- TX
            -------------+                       +   -------------
            -------------+                       +   -------------
            -------------+                       +   -------------
            -------------+                       +   -------------
            -------------+                       +   -------------
            -------------+                       +   -------------
            -------------+                       +   -------------
            -------------+                       +   -------------
            -------------+                       +   -------------
            -------------+                       +   -------------
            -------------+                       +   -------------
            -------------+                       +   -------------
            -------------+                       +   -------------
CAN TX      P0.1: CAN TX +                       +   CAN RX: P0.0   CAN RX
CMPS SDA  P0.10:I2C SDA2 +                       +   SCL2: P0.11    CMPS SCL
            GROUND       +                       +   GROUND
            +--------------------------------------+
*/

/*

SJ2 Board Pins
+------------------------------------+

P1.0: SCK2   +         +   VIn:   <5v
P1.1: MOSI2  +         +   Vcc:   3.3V
P1.4: MISO2  +         +   COMP:  P1.14
P4.28: TX3   +         +   RX3:   P4.29   UART3
P0.6: ----   +         +   SCK1:  P0.7
P0.8: MISO1  +         +   MOSI1: P0.9
P0.26: DAC   +         +   ADC2: P0.25  ADC pins
P1.31: ADC5  +         +   ADC4: P1.30  ADC pins
P1.20: OE1A  +         +   OE1B: P1.23
P1.28: CAP0  +         +   IrTX: P1.29
P2.0: PWM1   +         +   PWM2: P2.1
P2.2: PWM3   +         +   PWM5: P2.4
P2.5: PWM6   +         +   CAP0: P2.6
P2.7: CRD2   +         +   CT2D: P2.8   CAN, UART1
P2.9: IrRX   +         +   FI3:  P0.16
P0.15: SCK0  +         +   MISO: P0.17  SPI-0
P0.18: MOSI0 +         +   ----: P0.22
P0.1: SCL1   +         +   SDA1: P0.0   CAN, I2C1, UART3
P0.10: SDA2  +         +   SCL2: P0.11  UART2, I2C2
GROUND       +         +   GROUND
+--------------------------------------+
*/

/*
UART Pin Mapping

http://books.socialledge.com/link/88#bkmrk-sj2-uart%27s-txd-rxd-m

SJ2 UART's  TXD	    RXD	    Multiplexed
UART 0	    P0.2	P0.3	Bootloader, Serial Debug Output
'''''' 	    P0.0	P0.1	CAN1,I2C1

UART 1	    P0.15	P0.16	SSP0
''''''  	P2.0	P2.1	PWM1

UART 2	    P0.10	P0.11	I2C2
'''''' 	    P2.8	P2.9	Wi-Fi

UART 3	    P0.0	P0.1	CAN1,I2C1
''''''  	P0.25	P0.26	ADCx
''''''  	P4.28	P4.29	Wi-Fi

UART 4	    P0.22	P2.9
''''''  	P1.29	P2.9
*/

/*
SSP/SPI Pin Mapping for SJ-2 Board

http://books.socialledge.com/link/88#bkmrk-sj2-spi%27s-sck-miso-m

SJ2 SPI's	SCK	    MISO	MOSI
SSP0	    P0.15	P0.17	P0.18
'''' 	    P1.20	P1.23	P1.24

SSP1	    P0.7	P0.8	P0.9

SSP2	    P1.19	P1.18	P1.22
'''' 	    P1.31	P1.18	P1.22
*/

/*
I2C Pin Mapping for SJ-2 Board

http://books.socialledge.com/link/88#bkmrk-sj2-i2c%27s-sda-scl-mu

SJ2 I2C's	SDA	    SCL	    Multiplexed
I2C 0	    P1.30	P1.31	ADCx

I2C 1	    P0.0	P0.1	UART0, UART3, CAN1

I2C 2	    P0.10	P0.11	UART2
''''' 	    P1.15	P4.29
*/

/*

CAN Pin Mapping for SJ-2 Board

http://books.socialledge.com/link/88#bkmrk-sj2-can%27s-rd-td-mult

SJ2 CAN's	RD	    TD	    Multiplexed
CAN1	    P0.0	P0.1	UART0, I2C1, UART3

'''' 	    P0.0	P0.22

CAN2	    P2.7	P2.8
*/