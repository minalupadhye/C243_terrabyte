#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "common_macros.h"
#include "gpio.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"

int main(void) {

  puts("Starting RTOS");

  periodic_scheduler__initialize();

  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails

  return 0;
}
