#include "periodic_callbacks.h"
#include "board_io.h"
#include "compass2.h"

#include "geo_can_bus_initializer.h"
#include "geo_msg_bank.h"
#include "gpio.h"
#include "gps.h"
#include "led_handle.h"
#include "uart.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  // This method is invoked once when the periodic tasks are created
  // fake_gps__init();
  turn_off_all_leds();
  compass2__init();
  gps__init();

  my_can_init(can1);
}

void periodic_callbacks__1Hz(uint32_t callback_count) {}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  // gps__run_once();
  // msg_bank__handle_msg_tx();
  // msg_bank__handle_msg_rx();
}
void periodic_callbacks__100Hz(uint32_t callback_count) {

  if (callback_count % 5 == 0) {
    gps__run_once();
    msg_bank__handle_msg_tx();
    msg_bank__handle_msg_rx();
  }
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {}