#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockcompass2.h"
#include "Mockgeo_can_bus_initializer.h"
#include "Mockgeo_msg_bank.h"
#include "Mockgpio.h"
#include "Mockgps.h"
#include "Mockled_handle.h"

// Include the source we wish to test
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  const can__num_e expected_can = can1;
  turn_off_all_leds_Expect();
  compass2__init_Expect();
  gps__init_Expect();
  my_can_init_Expect(expected_can);
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) { periodic_callbacks__1Hz(0); }

// void test__periodic_callbacks__10Hz(void) {

//   gps__run_once_Expect();
//   config_gps_Expect();
//   msg_bank__handle_msg_tx_Expect();
//   msg_bank__handle_msg_rx_Expect();
//   periodic_callbacks__10Hz(0); // zeroth call

//   gps__run_once_Expect();
//   config_gps_Expect();

//   msg_bank__handle_msg_tx_Expect();
//   msg_bank__handle_msg_rx_Expect();
//   periodic_callbacks__10Hz(5); //  5th call
// }

// void test__periodic_callbacks__10Hz(void) {

//   gps__run_once_Expect();
//   msg_bank__handle_msg_tx_Expect();
//   msg_bank__handle_msg_rx_Expect();
//   periodic_callbacks__10Hz(0); // zeroth call
// }

void test__periodic_callbacks__100Hz(void) {

  for (uint8_t i = 0; i < 10; i++) {
    if (i % 5 == 0) {
      gps__run_once_Expect();
      msg_bank__handle_msg_tx_Expect();
      msg_bank__handle_msg_rx_Expect();
    }
    periodic_callbacks__100Hz(i);
  }
}
