#include "geo_can_bus_initializer.h"

#include <stdio.h>

#include "Mockcan_bus.h"
#include "unity.h"

void setUp(void) {}

void tearDown(void) {}

void test__my_can_init_for_can1(void) {
  can__init_ExpectAndReturn(can1, 500, 20, 20, NULL, NULL, true);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(can1);
  my_can_init(can1);
}
