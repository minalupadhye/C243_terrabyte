#include "dbc_to_can_geo_glue.h"

bool dbc_send_can_message(void *argument, uint32_t message_id, const uint8_t bytes[8], uint8_t dlc) {
  can__msg_t msg = {};

  msg.frame_fields.data_len = dlc;
  msg.msg_id = message_id;
  memcpy(msg.data.bytes, bytes, 8);

  return can__tx(can1, &msg, 0);
}
