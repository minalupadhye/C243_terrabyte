#include "project.h"

const uint32_t dbc_mia_threshold_SENSOR_DATA = 500;
const uint32_t dbc_mia_threshold_GEO_STATUS = 500;
const uint32_t dbc_mia_threshold_MOTOR_STATUS = 500;

const dbc_SENSOR_DATA_s dbc_mia_replacement_SENSOR_DATA = {};
const dbc_GEO_STATUS_s dbc_mia_replacement_GEO_STATUS = {};
const dbc_MOTOR_STATUS_s dbc_mia_replacement_MOTOR_STATUS = {};
