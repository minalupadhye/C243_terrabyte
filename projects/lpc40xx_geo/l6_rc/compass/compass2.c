#include "compass2.h"

#include "clock.h"
#include "gpio.h"
#include "i2c.h"
#include "lpc40xx.h"

#include <stdio.h>

#include "led_handle.h"

// ***********************************************************************
// *                             Private                                 *
// ***********************************************************************

static const uint8_t CMPS12_ADDRESS = 0xC0;
static const uint8_t CMPS12_BOSCH_ANGLE_BNO055 = 0x1A;
static const uint8_t CMPS12_BOSCH_SCALING_FACTOR = 16;
static const i2c_e i2c_bus_port_compass = I2C__2;
static const gpio__port_e I2C_PORT_COMPASS = GPIO__PORT_0;
static const uint8_t I2C_SDA_COMPASS_PIN = 10;
static const uint8_t I2C_SCL_COMPASS_PIN = 11;
static const gpio__function_e COMPASS_IOCON_ALT_FUNCTION = GPIO__FUNCTION_2;

static bool compass_heartbeat;

static float compass_heading_degrees;

static void enable_pull_up_resistor_for_i2c_compass_pins(void) {
  LPC_IOCON->P0_10 &= ~(0b11); // Clear the two least significant bits
  LPC_IOCON->P0_10 |= (0b10);  // Set the bits to enable pull-up resistor
  LPC_IOCON->P0_11 &= ~(0b11); // Clear the two least significant bits
  LPC_IOCON->P0_11 |= (0b10);  // Set the bits to enable pull-up resistor
}

bool static compass2__read_once_magnetometer_data(void) {
  const uint8_t COMPASS_HEADING_BYTES = 2;
  uint8_t received_raw_data[2] = {0};
  compass_heartbeat = false;

  compass_heartbeat = i2c__read_slave_data(i2c_bus_port_compass, CMPS12_ADDRESS, CMPS12_BOSCH_ANGLE_BNO055,
                                           received_raw_data, COMPASS_HEADING_BYTES);

  if (compass_heartbeat) {
    compass_heading_degrees = ((float)(((uint16_t)received_raw_data[0] << 8) | ((uint16_t)received_raw_data[1]))) /
                              CMPS12_BOSCH_SCALING_FACTOR;
  }

  return compass_heartbeat;
}

float static compass2__get_heading_degrees(void) {
  // printf("cmpass 2 float:%f ", compass_heading_degrees);
  return compass_heading_degrees;
}

// ***********************************************************************
// *                             Public                                  *
// ***********************************************************************

void compass2__init(void) {

  // sets P0.10 and P0.11 pins on the SJ2 Board as I2C Data and SCL respectively
  static StaticSemaphore_t binary_sem_struct, mutex_struct;
  const uint32_t i2c_bus_speed = UINT32_C(400) * 1000;

  gpio__construct_with_function(I2C_PORT_COMPASS, I2C_SDA_COMPASS_PIN, COMPASS_IOCON_ALT_FUNCTION);
  gpio__construct_with_function(I2C_PORT_COMPASS, I2C_SCL_COMPASS_PIN, COMPASS_IOCON_ALT_FUNCTION);

  enable_pull_up_resistor_for_i2c_compass_pins();

  i2c__initialize(I2C__2, i2c_bus_speed, clock__get_peripheral_clock_hz(), &binary_sem_struct, &mutex_struct);
}

uint16_t compass2__get_heading() {

  uint16_t unsigned_degrees = 0;

  if (compass2__read_once_magnetometer_data()) {

    unsigned_degrees = (uint16_t)(compass2__get_heading_degrees());

  } else {
    // printf("Error: could not fetch compass data \n\n");
  }
  return unsigned_degrees;
}

bool compass2__heartbeat(void) { return compass_heartbeat; }