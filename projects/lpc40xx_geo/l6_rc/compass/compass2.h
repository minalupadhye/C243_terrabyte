#pragma once

#include <stdbool.h>
#include <stdint.h>

void compass2__init(void);
uint16_t compass2__get_heading(void);
bool compass2__heartbeat(void);