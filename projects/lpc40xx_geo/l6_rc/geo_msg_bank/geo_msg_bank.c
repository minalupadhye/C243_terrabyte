#include "geo_msg_bank.h"
#include "compass2.h"
#include "geo_status.h"
#include "led_handle.h"

#include <math.h>
#include <stdio.h>

// ***********************************************************************
// *                             Private                                 *
// ***********************************************************************

static dbc_GPS_DESTINATION_LOCATION_s bridge_data = {0};

// ***********************************************************************
// *                             Public                                  *
// ***********************************************************************

void msg_bank__handle_msg_tx(void) {
  dbc_GEO_STATUS_s tx_can_msg_geo_status = {0};
  dbc_GPS_CURRENT_LOCATION_s tx_can_current_coordinates = {0};
  dbc_DEBUG_GEO_s tx_can_geo_debug = {0};

  geo_get_status(&tx_can_msg_geo_status);
  dbc_encode_and_send_GEO_STATUS(NULL, &tx_can_msg_geo_status);
  printf("sending GPS status message: distance: %lf  , compass bearing: %u  , compass heading: %u \n",
         tx_can_msg_geo_status.GEO_STATUS_DISTANCE_TO_DESTINATION, tx_can_msg_geo_status.GEO_STATUS_COMPASS_BEARING,
         tx_can_msg_geo_status.GEO_STATUS_COMPASS_HEADING);

  geo_get_current_location_coordinates(&tx_can_current_coordinates);
  dbc_encode_and_send_GPS_CURRENT_LOCATION(NULL, &tx_can_current_coordinates);
  // printf("sending GPS current location : Latitude_scaled: %ld  , Longitude_scaled: %ld \n\n\n",
  //        tx_can_current_coordinates.GPS_CURR_LATITUDE_SCALED_1000000,
  //        tx_can_current_coordinates.GPS_CURR_LONGITUDE_SCALED_1000000);

  tx_can_geo_debug.GEO_Compass_heartbeat = compass2__heartbeat();
  dbc_encode_and_send_DEBUG_GEO(NULL, &tx_can_geo_debug);
}

void msg_bank__handle_msg_rx(void) {
  can__msg_t rx_can_msg_bridge_data = {};

  // Receive all messages
  while (can__rx(can1, &rx_can_msg_bridge_data, 0)) {
    const dbc_message_header_t header = {
        .message_id = rx_can_msg_bridge_data.msg_id,
        .message_dlc = rx_can_msg_bridge_data.frame_fields.data_len,
    };

    if (dbc_decode_GPS_DESTINATION_LOCATION(&bridge_data, header, rx_can_msg_bridge_data.data.bytes)) {
      // printf("bridge data received and decoded.\n");
      // printf("scaled data: Latittude:%ld , Longitude: %ld \n\n\n ", bridge_data.GPS_DEST_LATITUDE_SCALED_1000000,
      //        bridge_data.GPS_DEST_LONGITUDE_SCALED_1000000);
    }
  }
}

void msg_bank__service_mia_10hz(void) {
  const uint32_t mia_increment_value_for_10Hz = 100;

  if (dbc_service_mia_GPS_DESTINATION_LOCATION(&bridge_data, mia_increment_value_for_10Hz)) {
    // printf("Bridge values frame MIA.\n");
  } else {
    turn_off_led_3();
  }
}

dbc_GPS_DESTINATION_LOCATION_s msg_bank__get_bridge_data(void) { return bridge_data; }
