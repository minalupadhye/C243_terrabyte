#pragma once

#include "can_bus.h"
#include "project.h"

typedef struct {
  dbc_mia_info_t mia_info;

  float GPS_DEST_LATITUDE_SCALED_1000000;  // unit: Degrees
  float GPS_DEST_LONGITUDE_SCALED_1000000; // unit: Degrees
} descaled_dbc_GPS_DESTINATION_LOCATION_s;

// Invoke this method in your periodic callbacks
void msg_bank__handle_msg_rx(void);
void msg_bank__handle_msg_tx(void);

// MIA management
void msg_bank__service_mia_10hz(void);

// "GET" APIs to obtain latest data
dbc_GPS_DESTINATION_LOCATION_s msg_bank__get_bridge_data(void);

// "PUT" APIs to send data to geo data to driver
dbc_GEO_STATUS_s msg_bank__put_gps_data(void);
