#include "geo_status.h"

#include "compass2.h"
#include "geo_msg_bank.h"
#include "gps.h"

#include <math.h>
#include <stdio.h>

#define GEO_STATUS_DEBUG 0

// ***********************************************************************
// *                             Private                                 *
// ***********************************************************************

static const float new_M_PI = 3.14159265358979323846;
static const uint32_t RADIUS_EARTH_KM = 6371e3;
static gps_coordinates_t gps_destination;
static bool distace_zero_true = false;

static float helper_convert_to_radian(float degree_value) { return degree_value * (new_M_PI / 180.0f); }

static float helper_find_haversine_angle(gps_coordinates_t coordinate_1, gps_coordinates_t coordinate_2) {
  float haversine_angle = 0.0;

  float latitude_coordinate_1_radian = helper_convert_to_radian(coordinate_1.latitude);
  float longitude_coordinate_1_radian = helper_convert_to_radian(coordinate_1.longitude);
  float latitude_coordinate_2_radian = helper_convert_to_radian(coordinate_2.latitude);
  float longitude_coordinate_2_radian = helper_convert_to_radian(coordinate_2.longitude);

  float delta_latitude = latitude_coordinate_2_radian - latitude_coordinate_1_radian;
  float delta_longitude = longitude_coordinate_2_radian - longitude_coordinate_1_radian;

  haversine_angle = (sin(delta_latitude / 2) * sin(delta_latitude / 2)) +
                    ((cos(latitude_coordinate_1_radian) * cos(latitude_coordinate_2_radian)) *
                     ((sin(delta_longitude / 2) * sin(delta_longitude / 2))));

#if (GEO_STATUS_DEBUG)
  printf("*****helper_find_haversine_angle****()\n");
  printf("lat 1 :%f \n", coordinate_1.latitude);
  printf("lat 1 rad:%f \n", latitude_coordinate_1_radian);

  printf("long 1 :%f \n", coordinate_1.longitude);
  printf("long 1 rad:%f \n", longitude_coordinate_1_radian);

  printf("lat 2 :%f \n", coordinate_2.latitude);
  printf("lat 2 rad:%f \n", latitude_coordinate_2_radian);

  printf("long 2 :%f \n", coordinate_2.longitude);
  printf("long 2 rad:%f \n", longitude_coordinate_2_radian);

  printf("haversine angle:%lf  \n", haversine_angle);
#endif

  return haversine_angle;
}

static float find_distance(float haversine_angle) {
  float distance = 0.0f;
  distance = 2 * RADIUS_EARTH_KM * asin(sqrt(haversine_angle));

#if (GEO_STATUS_DEBUG)
  printf("*****find_distance****()\n");
  printf("sqrt of angle :%lf \n", sqrt(haversine_angle));
  printf(" arc sin of sqrt of angle :%lf \n", asin(sqrt(haversine_angle)));
  printf("distance :%lf \n", distance);
#endif

  return distance;
}

static uint16_t find_bearing(gps_coordinates_t coordinate_1, gps_coordinates_t coordinate_2) {
  uint16_t bearing = 0;
  float latitude_coordinate_1_radian = helper_convert_to_radian(coordinate_1.latitude);
  float longitude_coordinate_1_radian = helper_convert_to_radian(coordinate_1.longitude);

  float latitude_coordinate_2_radian = helper_convert_to_radian(coordinate_2.latitude);
  float longitude_coordinate_2_radian = helper_convert_to_radian(coordinate_2.longitude);

  float delta_longitude = longitude_coordinate_2_radian - longitude_coordinate_1_radian;

  float y = sin(delta_longitude) * cos(latitude_coordinate_2_radian);

  float x = (cos(latitude_coordinate_1_radian) * sin(latitude_coordinate_2_radian)) -
            (sin(latitude_coordinate_1_radian) * cos(latitude_coordinate_2_radian) * cos(delta_longitude));

  float bearing_radian = atan2(y, x);

  bearing = (uint16_t)((bearing_radian * 180.0f / new_M_PI) + 360) % 360;

#if (GEO_STATUS_DEBUG)
  printf("*****find_bearing()****()\n");
  printf("Y: %f\n", y);
  printf("X: %f\n", x);
  printf("bearing_radian: %f\n", bearing_radian);
  printf("bearing: %u\n", bearing);
#endif

  return bearing;
}

// ***********************************************************************
// *                             Public                                  *
// ***********************************************************************

void geo_get_status(dbc_GEO_STATUS_s *msg) {
  float distance = 0.0f;
  uint16_t bearing = 0;
  uint16_t heading = 0;
  float haversine_angle = 0.0f;

  // get current coordinates
  gps_coordinates_t gps_current_reading = {0};
  gps_current_reading = gps__get_coordinates();
  bool geo_dest_valid = geo_get_destination_coordinates();
  bool geo_current_valid = gps_current_reading.valid;
  if (geo_current_valid && geo_dest_valid) {
    // get the destination co-ordinates
    haversine_angle = helper_find_haversine_angle(gps_current_reading, gps_destination);

    distance = find_distance(haversine_angle);

    bearing = find_bearing(gps_current_reading, gps_destination);
    if (distance <= 3.0f) {
    distace_zero_true = true;
  }
  } else if (geo_current_valid && !geo_dest_valid) // in case bridge_sensor's CAN gets aribtirated
  {
    haversine_angle = helper_find_haversine_angle(gps_current_reading, gps_destination);

    distance = find_distance(haversine_angle);

    bearing = find_bearing(gps_current_reading, gps_destination);
      if (distance <= 3.0f) {
      distace_zero_true = true;
    }
  }
  

  heading = compass2__get_heading();

  // set message signals
  msg->GEO_STATUS_DISTANCE_TO_DESTINATION = distance;
  msg->GEO_STATUS_COMPASS_HEADING = heading;
  msg->GEO_STATUS_COMPASS_BEARING = bearing;
  msg->GEO_STATUS_VALID_FLAG = geo_current_valid;
  msg->GEO_STATUS_DESTINATION_REACHED = distace_zero_true;
}

bool geo_get_destination_coordinates() {
  bool ret_val = false;
  dbc_GPS_DESTINATION_LOCATION_s destination_coordinates = msg_bank__get_bridge_data();

  if ((!destination_coordinates.GPS_DEST_LATITUDE_SCALED_1000000) &&
      (!destination_coordinates.GPS_DEST_LONGITUDE_SCALED_1000000)) {
    // printf("bridge data not recieved\n\n\n");
  } else {
    ret_val = true;
    gps_destination.latitude = (destination_coordinates.GPS_DEST_LATITUDE_SCALED_1000000 / 1000000.0);
    gps_destination.longitude = (destination_coordinates.GPS_DEST_LONGITUDE_SCALED_1000000 / 1000000.0);

#if (GEO_STATUS_DEBUG)
    printf("*****geo_get_destination_coordinates()****");
    printf("de-scaled data from bridge: Latittude:%lf , Longitude: %lf \n\n\n ", gps_destination.latitude,
           gps_destination.longitude);
#endif
  }
  return ret_val;
}

bool geo_get_current_location_coordinates(dbc_GPS_CURRENT_LOCATION_s *current_coordinates_scaled) {
  bool ret_value = false;
  gps_coordinates_t gps_current = {};
  gps_current = gps__get_coordinates();
  if (gps_current.valid) {

    // printf("got curr co-ordinates: Latitude=%lf Longitude=%lf\n\n\n", gps_current.latitude, gps_current.longitude);
    current_coordinates_scaled->GPS_CURR_LATITUDE_SCALED_1000000 = gps_current.latitude * 100000;
    current_coordinates_scaled->GPS_CURR_LONGITUDE_SCALED_1000000 = gps_current.longitude * 100000;
    ret_value = true;
  }
#if (GEO_STATUS_DEBUG)
  printf("*****geo_get_current_location_coordinates****()\n");
  printf("current co-ordinates_scaled: Latitude=%ld Longitude=%ld\n\n\n",
         current_coordinates_scaled->GPS_CURR_LATITUDE_SCALED_1000000,
         current_coordinates_scaled->GPS_CURR_LONGITUDE_SCALED_1000000);
#endif
  return ret_value;
}
