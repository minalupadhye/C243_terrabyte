#pragma once

#include "project.h"

void geo_get_status(dbc_GEO_STATUS_s *msg);
bool geo_get_destination_coordinates();
bool geo_get_current_location_coordinates(dbc_GPS_CURRENT_LOCATION_s *current_coordinates_scaled);