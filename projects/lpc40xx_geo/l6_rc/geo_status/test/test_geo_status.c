#include "Mockcompass2.h"
#include "Mockgeo_msg_bank.h"
#include "Mockgps.h"
#include "cmock.h"
#include "geo_status.c"

#include "unity.h"
#include <math.h>

void setUp(void) {}

void tearDown(void) {}

void test_helper_convert_to_radian_expect_zero_when_angle_zero(void) {
  TEST_ASSERT_EQUAL_FLOAT(helper_convert_to_radian(0.0f), 0.0f);
}

void test_helper_convert_to_radian_expect_PI_when_angle_90(void) {

  TEST_ASSERT_EQUAL_FLOAT(helper_convert_to_radian(90), (new_M_PI / 2));
}

void test_helper_convert_to_radian_expect_PI_when_angle_negative_90(void) {

  TEST_ASSERT_EQUAL_FLOAT(helper_convert_to_radian(-90), -1 * (new_M_PI / 2));
}

void test_helper_convert_to_radian_expect_PI_when_angle_180(void) {

  TEST_ASSERT_EQUAL_FLOAT(helper_convert_to_radian(180), new_M_PI);
}

void test_helper_convert_to_radian_expect_PI_when_angle_negative_180(void) {

  TEST_ASSERT_EQUAL_FLOAT(helper_convert_to_radian(-180), -1 * new_M_PI);
}

void test_helper_convert_to_radian_expect_PIx2_when_angle_360(void) {

  TEST_ASSERT_EQUAL_FLOAT(helper_convert_to_radian(360), 2 * new_M_PI);
}

void test_helper_convert_to_radian_expect_PIx2_when_angle__negative_360(void) {

  TEST_ASSERT_EQUAL_FLOAT(helper_convert_to_radian(-360), -1 * (2 * new_M_PI));
}

void test_helper_convert_to_radian_expect_PIx2_when_angle_over_360(void) {
  // 360 + 90
  TEST_ASSERT_EQUAL_FLOAT(helper_convert_to_radian(450), (2.5f * new_M_PI));
}

void test_helper_convert_to_radian_expect_PIx2_when_angle_over_negaitve_360(void) {
  //-360 - 90
  TEST_ASSERT_EQUAL_FLOAT(helper_convert_to_radian(-450), -1 * (2.5f * new_M_PI));
}

void test_helper_convert_to_radian_expect_zero_when_angle_very_small(void) {
  float angle_degree = 0.001f;
  float expected_radian = angle_degree * (new_M_PI / 180.0f);
  TEST_ASSERT_FLOAT_WITHIN(1e-6, expected_radian, helper_convert_to_radian(angle_degree));
}

void test_helper_convert_to_radian_precision_at_boundary(void) {
  float angle_degrees = 179.999f;
  float expected_radians = angle_degrees * (new_M_PI / 180.0f);
  TEST_ASSERT_FLOAT_WITHIN(1e-6, expected_radians, helper_convert_to_radian(angle_degrees));
}

void test_helper_find_haversine_angle_expect_0_when_coordintates_0(void) {
  gps_coordinates_t coordinate_1 = {0.0f, 0.0f};
  gps_coordinates_t coordinate_2 = {0.0f, 0.0f};

  TEST_ASSERT_EQUAL_FLOAT(helper_find_haversine_angle(coordinate_1, coordinate_2), 0.0f);
}

void test_helper_find_haversine_angle_same_point_expect_0(void) {
  gps_coordinates_t coordinate_1 = {0.0f, 0.0f};
  gps_coordinates_t coordinate_2 = {0.0f, 0.0f};

  float expected_haversine_angle = 0.0f;

  float calculated_haversine_angle = helper_find_haversine_angle(coordinate_1, coordinate_2);
  TEST_ASSERT_FLOAT_WITHIN(1e-6, expected_haversine_angle, calculated_haversine_angle);
}

void test_helper_find_haversine_angle_antipodal_points_equator(void) {
  gps_coordinates_t coordinate_1 = {0.0f, 0.0f};
  gps_coordinates_t coordinate_2 = {0.0f, 180.0f}; // Antipodal point

  float expected_haversine_angle = 1.0f; //  1 (maximum angle)

  float calculated_haversine_angle = helper_find_haversine_angle(coordinate_1, coordinate_2);
  TEST_ASSERT_FLOAT_WITHIN(1e-6, expected_haversine_angle, calculated_haversine_angle);
}

void test_helper_find_haversine_angle_antipodal_poles(void) {
  gps_coordinates_t coordinate_1 = {90.0, 0.0};  // North Pole
  gps_coordinates_t coordinate_2 = {-90.0, 0.0}; // South Pole

  float expected_haversine_angle = 1.0;

  float calculated_haversine_angle = helper_find_haversine_angle(coordinate_1, coordinate_2);
  TEST_ASSERT_FLOAT_WITHIN(1e-6, expected_haversine_angle, calculated_haversine_angle);
}

void test_helper_find_haversine_angle__non_zero_longitude_antipodal(void) {
  gps_coordinates_t coordinate_1 = {45.0, 67.0};    // random location
  gps_coordinates_t coordinate_2 = {-45.0, -113.0}; // Exact antipode

  float expected_haversine_angle = 1.0;

  float calculated_haversine_angle = helper_find_haversine_angle(coordinate_1, coordinate_2);
  TEST_ASSERT_FLOAT_WITHIN(1e-6, expected_haversine_angle, calculated_haversine_angle);
}

void test_helper_find_haversine_angle__city_based_antipodal(void) {
  gps_coordinates_t coordinate_1 = {39.91, 116.36};  // Beijing, China
  gps_coordinates_t coordinate_2 = {-39.91, -63.64}; // Near Buenos Aires, Argentina

  float expected_haversine_angle = 1.0; // Approx maximum haversine angle

  float calculated_haversine_angle = helper_find_haversine_angle(coordinate_1, coordinate_2);
  TEST_ASSERT_FLOAT_WITHIN(1e-6, expected_haversine_angle, calculated_haversine_angle);
}

void test_helper_find_haversine_angle__betwen_krakow_and_paris(void) {
  float delta = 0.00001;
  gps_coordinates_t paris = {48.8566, 2.3522};
  gps_coordinates_t krakow = {50.0647, 19.9450};

  TEST_ASSERT_FLOAT_WITHIN(delta, 0.009988, helper_find_haversine_angle(krakow, paris));
}

void test_helper_find_haversine_angle__betwen_EmpireStateBuilding_and_MtEverest(void) {
  float delta = 0.00001;
  gps_coordinates_t EmpireStateBuilding = {40.7484, -73.9857};
  gps_coordinates_t MtEverest = {27.9881, 86.9250};

  TEST_ASSERT_FLOAT_WITHIN(delta, 0.662935, helper_find_haversine_angle(EmpireStateBuilding, MtEverest));
}

void test_helper_find_haversine_angle__expect_greater_than_0(void) {

  gps_coordinates_t coordinate_1 = {0.0f, 1.0f};
  gps_coordinates_t coordinate_2 = {1.0f, 1.0f};

  float expected_angle = helper_find_haversine_angle(coordinate_1, coordinate_2);
  TEST_ASSERT_TRUE(expected_angle > 0.0f);
}

void test_find_distance__zero_angle(void) {
  float haversine_angle = 0.0;
  float expected_distance = 0.0;

  float calculated_distance = find_distance(haversine_angle);
  TEST_ASSERT_FLOAT_WITHIN(1e-6, expected_distance, calculated_distance);
}

void test_find_distance__max_angle(void) {
  float haversine_angle = 1.0f;                         // antipodal points
  float expected_distance = new_M_PI * RADIUS_EARTH_KM; // Should be close to half the circumference of Earth
  float calculated_distance = find_distance(haversine_angle);
  TEST_ASSERT_FLOAT_WITHIN(5.0f, expected_distance, calculated_distance);
}

void test_find_distance__between_paris_krakow(void) {
  float expected_distance = 1275.6 * 1000; // meters
  float known_haversine_angle = 0.009988;
  float calculated_distance = find_distance(known_haversine_angle);
  TEST_ASSERT_FLOAT_WITHIN(40.0, expected_distance, calculated_distance);
}

void test_find_distance__between_EmpireStateBuilding_and_MtEverest(void) {
  float expected_distance = 12122 * 1000; // meters
  float known_haversine_angle = 0.662935;
  float calculated_distance = find_distance(known_haversine_angle);
  TEST_ASSERT_FLOAT_WITHIN(300.0, expected_distance, calculated_distance);
}

void test_find_bearing_north(void) {
  gps_coordinates_t coordinate_1 = {0.0f, 0.0f};  // Equator, Prime Meridian
  gps_coordinates_t coordinate_2 = {10.0f, 0.0f}; // North along the Prime Meridian
  uint16_t calculated_bearing = find_bearing(coordinate_1, coordinate_2);
  uint16_t expected_bearing = 0;
  TEST_ASSERT_EQUAL_UINT16(expected_bearing, calculated_bearing);
}

void test_find_bearing_east(void) {
  gps_coordinates_t coordinate_1 = {0.0, -10.0}; // Equator, West of Prime Meridian
  gps_coordinates_t coordinate_2 = {0.0, 10.0};  // Equator, East of Prime Meridian
  uint16_t calculated_bearing = find_bearing(coordinate_1, coordinate_2);
  uint16_t expected_bearing = 90; // Expected to be east
  TEST_ASSERT_EQUAL_UINT16(expected_bearing, calculated_bearing);
}

void test_find_bearing_south(void) {
  gps_coordinates_t coordinate_1 = {10.0, 0.0}; // North of Equator, Prime Meridian
  gps_coordinates_t coordinate_2 = {0.0, 0.0};  // Equator, Prime Meridian
  uint16_t calculated_bearing = find_bearing(coordinate_1, coordinate_2);
  uint16_t expected_bearing = 180; // Expected to be south
  TEST_ASSERT_EQUAL_UINT16(expected_bearing, calculated_bearing);
}

void test_find_bearing_west(void) {
  gps_coordinates_t coordinate_1 = {0.0, 10.0};  // Equator, East of Prime Meridian
  gps_coordinates_t coordinate_2 = {0.0, -10.0}; // Equator, West of Prime Meridian
  uint16_t calculated_bearing = find_bearing(coordinate_1, coordinate_2);
  uint16_t expected_bearing = 270; // Expected to be west
  TEST_ASSERT_EQUAL_UINT16(expected_bearing, calculated_bearing);
}

void test_find_bearing_wrapping_north(void) {
  gps_coordinates_t coordinate_1 = {89.9f, -1.0f}; // Near North Pole, just west of Prime Meridian
  gps_coordinates_t coordinate_2 = {89.9f, 1.0f};  // Near North Pole, just east of Prime Meridian
  uint16_t calculated_bearing = find_bearing(coordinate_1, coordinate_2);
  uint16_t expected_bearing = 89; // Close to north, wrapping around
  TEST_ASSERT_EQUAL_UINT16(expected_bearing, calculated_bearing);
}

void test_find_bearing_cross_prime_meridian(void) {
  gps_coordinates_t coordinate_1 = {0.0, -0.1}; // Equator, just west of Prime Meridian
  gps_coordinates_t coordinate_2 = {0.0, 0.1};  // Equator, just east of Prime Meridian
  uint16_t calculated_bearing = find_bearing(coordinate_1, coordinate_2);
  uint16_t expected_bearing = 90; // Expected to be directly east
  TEST_ASSERT_EQUAL_UINT16(expected_bearing, calculated_bearing);
}
