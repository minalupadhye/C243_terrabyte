#include "gps.h"

#include "clock.h"
#include "delay.h"
#include "gpio.h"
#include "led_handle.h"
#include "line_buffer.h"
#include "uart.h"

#include <stdio.h>
#include <stdlib.h>

#define GPS_PARSE_DEBUG 0

// ***********************************************************************
// *                             Private                                 *
// ***********************************************************************

static const uart_e gps_uart = UART__3;

static char line_buffer[200];
static line_buffer_s line;
static bool gps_fix;
static gps_coordinates_t parsed_coordinates;

static void gps__transfer_data_from_uart_driver_to_line_buffer(void) {
  char byte;
  const uint32_t zero_timeout = 0;

  while (uart__get(gps_uart, &byte, zero_timeout)) {
    line_buffer__add_byte(&line, byte);
  }
}

static float convert_from_minutes_to_decimal(float degrees_minutes) {
  int degrees = (int)(degrees_minutes / 100);
  float minutes = degrees_minutes - (degrees * 100);
  return degrees + (minutes / 60);
}

static void turn_on_gps_fix_led(void) { turn_on_led_0(); }

static void turn_off_gps_fix_led(void) { turn_off_led_0(); }

static bool gps__parse_coordinates_from_line(void) {
  char gps_line[200];
  bool return_value = false;
  uint8_t field_count = 0;

  if (line_buffer__remove_line(&line, gps_line, sizeof(gps_line))) {
    // printf("Received NMEA Sentence from gps module: %s\n", gps_line);

    // set co-ordinate as invalid initially and gps-fix to false
    parsed_coordinates.valid = false;
    gps_fix = false;

    // copy to reparse
    char gps_line_copy[200];
    strncpy(gps_line_copy, gps_line, sizeof(gps_line_copy));

    // parse and get the preamble
    char *token = strtok(gps_line_copy, ",");
    field_count++;
    // printf("1st strtok $ %s\n", token);

    // Check if the line is a GPGGA sentence
    if (token && strcmp(token, "$GPGGA") == 0) {
      // Skip the time field which is the second token
      char *time = strtok(NULL, ",");
      field_count++;

#if (GPS_PARSE_DEBUG)
      printf("2nd strtok time %s\n", (time && strlen(time) > 0) ? time : "empty");
#endif

      // skip other fields to first check fix
      for (int i = 0; i < 5; ++i) {
        token = strtok(NULL, ",");
        field_count++;

#if (GPS_PARSE_DEBUG)
        printf("%dth strtok %s\n", i + 3, (token && strlen(token) > 0) ? token : "empty");
#endif

        if (token == NULL) {
          // printf("Incomplete NMEA sentence.\n");
          return_value = false;
          break;
        }
      }

      if (token && field_count == 7) {
        int fix_quality = atoi(token);
        if (fix_quality > 0) {
          gps_fix = true;
          turn_on_gps_fix_led();
          // GPS fix, parse latitiude and longititude
          // Re-parse the original line for latitude and longitude
          strtok(gps_line, ","); // skip $GPGGA
          strtok(NULL, ",");     // skip Time
          char *latitude = strtok(NULL, ",");
          char *latitude_direction = strtok(NULL, ",");
          char *longitude = strtok(NULL, ",");
          char *longitude_direction = strtok(NULL, ",");

          if (latitude && longitude && latitude_direction && longitude_direction) {
            // Convert and adjust coordinates
            float parsed_latitude = convert_from_minutes_to_decimal(atof(latitude));
            float parsed_longitude = convert_from_minutes_to_decimal(atof(longitude));

            if (*latitude_direction == 'S') {
              parsed_latitude *= -1.0f;
            }
            if (*longitude_direction == 'W') {
              parsed_longitude *= -1.0f;
            }

            parsed_coordinates.latitude = parsed_latitude;
            parsed_coordinates.longitude = parsed_longitude;
            parsed_coordinates.valid = true;
            return_value = true;
            // printf("Coordinates: Latitude=%f Longitude=%f\n", parsed_latitude, parsed_longitude);
          } else {
            // printf("Not enough data to parse coordinates.\n");
            return_value = false;
          }
        } else {
          // printf("No GPS fix available.\n");
          turn_off_gps_fix_led();
          gps_fix = false;
          return_value = false;
        }
      }
    } else {
      // printf("Not a GPGGA sentence.\n");
      return_value = false;
    }
  } else {
    // printf("Failed to remove line from buffer.\n");
    return_value = false;
  }

  return return_value;
}

// ***********************************************************************
// *                             Public                                  *
// ***********************************************************************

void config_gps(void) {

  printf("Configuring GPS\n");

  // const char set_baud_rate_cmd[] = "$PMTK251,38400*27\r\n";
  // const int baud_rate_cmd_length = strlen(set_baud_rate_cmd);
  // for (int i = 0; i < baud_rate_cmd_length; i++) {
  //   uart__put(gps_uart, set_baud_rate_cmd[i], 0);
  // }

  // uart__init(gps_uart, clock__get_peripheral_clock_hz(), 38400);
  // while (!uart__is_initialized(gps_uart))
  //   ;

  const char gps_config_only_gpgga_cmd[] = "$PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n";
  const int actual_print = strlen(gps_config_only_gpgga_cmd);
  for (int i = 0; i < actual_print; i++) {
    uart__put(gps_uart, gps_config_only_gpgga_cmd[i], 0);
  }
}

void gps__init(void) {
  const static gpio__port_e port_gps_uart = GPIO__PORT_4;
  const static uint8_t gps_uart_tx_pin = 28; // P4.28 - Uart-3 Tx
  const static uint8_t gps_uart_rx_pin = 29; // P4.29 - Uart-3 Rx
  const static gpio__function_e lpc_iocon_func_gps_pin = GPIO__FUNCTION_2;

  line_buffer__init(&line, line_buffer, sizeof(line_buffer));
  uart__init(gps_uart, clock__get_peripheral_clock_hz(), 9600);
  gpio__construct_with_function(port_gps_uart, gps_uart_tx_pin, lpc_iocon_func_gps_pin);
  gpio__construct_with_function(port_gps_uart, gps_uart_rx_pin, lpc_iocon_func_gps_pin);
  QueueHandle_t rxq_handle = xQueueCreate(100, sizeof(char));
  QueueHandle_t txq_handle = xQueueCreate(75, sizeof(char));
  uart__enable_queues(gps_uart, rxq_handle, txq_handle);
  delay__ms(20);
  config_gps();
}

void gps__run_once(void) {

  gps__transfer_data_from_uart_driver_to_line_buffer();
  gps__parse_coordinates_from_line();
  // if (!gps__parse_coordinates_from_line()) {
  //   printf("GPS parse function Error, check above\n");
  // }
}

gps_coordinates_t gps__get_coordinates(void) { return parsed_coordinates; }