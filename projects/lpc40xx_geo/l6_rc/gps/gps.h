#pragma once
#include <stdbool.h>
// Note:
// South means negative latittude
// West means negative longitutde
typedef struct {
  float latitude;
  float longitude;
  bool valid;
} gps_coordinates_t;

void gps__init(void);
void gps__run_once(void);
void config_gps(void);
gps_coordinates_t gps__get_coordinates(void);