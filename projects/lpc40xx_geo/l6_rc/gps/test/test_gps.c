#include "gps.c"

#include "Mockclock.h"
#include "Mockdelay.h"
#include "Mockgpio.h"
#include "Mockled_handle.h"
#include "Mockline_buffer.h"
#include "Mockqueue.h"
#include "Mockuart.h"
#include "cmock.h"
#include "unity.h"

#include <math.h>

void setUp(void) {
  parsed_coordinates.latitude = 0.0f;
  parsed_coordinates.longitude = 0.0f;
  parsed_coordinates.valid = false;
  gps_fix = false;
}

void tearDown(void) {}

// static void test_gps_parse_coordinates_from_valid_gpgga_line(void) {
//   char gps_line[200]; // Ensure the buffer is appropriately sized
//   char test_string[] = "$GPGGA,194530.000,3051.8007,N,10035.9989,W,1,4,2.18,746.4,M,-22.2,M,,*6B";
//   line_buffer__remove_line_ExpectAndReturn(&line, gps_line, sizeof(gps_line), true);
//   line_buffer__remove_line_IgnoreArg_buffer();
//   line_buffer__remove_line_IgnoreArg_line();
//   line_buffer__remove_line_IgnoreArg_line_max_size();
//   line_buffer__remove_line_ReturnThruPtr_line(test_string);

//   TEST_ASSERT_TRUE(gps__parse_coordinates_from_line());
//   TEST_ASSERT_FLOAT_WITHIN(0.0001, 30.863345f, parsed_coordinates.latitude);
//   TEST_ASSERT_FLOAT_WITHIN(0.0001, -100.599982f, parsed_coordinates.longitude);
//   TEST_ASSERT_TRUE(parsed_coordinates.valid);
//   TEST_ASSERT_TRUE(gps_fix);
// }

// void test_gps_parse_coordinates_from_valid_gpgga_line_1(void) {
//   char test_string[] = "$GPGGA,065922.000,3719.8312,N,12154.3158,W,1,07,1.10,21.5,M,-25.7,M,,*60";

//   line_buffer__remove_line_ExpectAndReturn(NULL, NULL, 0, true);
//   line_buffer__remove_line_IgnoreArg_buffer();
//   line_buffer__remove_line_IgnoreArg_line();
//   line_buffer__remove_line_IgnoreArg_line_max_size();
//   line_buffer__remove_line_ReturnThruPtr_line(test_string);

//   TEST_ASSERT_TRUE(gps__parse_coordinates_from_line());
//   TEST_ASSERT_FLOAT_WITHIN(0.0001, 37.33052f, parsed_coordinates.latitude);
//   TEST_ASSERT_FLOAT_WITHIN(0.0001, -121.905263f, parsed_coordinates.longitude);
// }

// void test_gps_parse_coordinates_from_valid_gpgga_line_2(void) {
//   char test_string[] = "$GPGGA,065923.000,3719.8319,N,12154.3167,W,1,07,1.10,23.8,M,-25.7,M,,*69";

//   line_buffer__remove_line_ExpectAndReturn(NULL, NULL, 0, true);
//   line_buffer__remove_line_IgnoreArg_buffer();
//   line_buffer__remove_line_IgnoreArg_line();
//   line_buffer__remove_line_IgnoreArg_line_max_size();
//   line_buffer__remove_line_ReturnThruPtr_line(test_string);

//   TEST_ASSERT_TRUE(gps__parse_coordinates_from_line());
//   TEST_ASSERT_FLOAT_WITHIN(0.0001, 37.330532f, parsed_coordinates.latitude);
//   TEST_ASSERT_FLOAT_WITHIN(0.0001, -121.905278f, parsed_coordinates.longitude);
// }

// void test_gps_parse_coordinates_from_valid_gpgga_line_3(void) {
//   char test_string[] = "$GPGGA,065924.000,3719.8309,N,12154.3155,W,1,07,1.10,24.8,M,-25.7,M,,*69";

//   line_buffer__remove_line_ExpectAndReturn(NULL, NULL, 0, true);
//   line_buffer__remove_line_IgnoreArg_buffer();
//   line_buffer__remove_line_IgnoreArg_line();
//   line_buffer__remove_line_IgnoreArg_line_max_size();
//   line_buffer__remove_line_ReturnThruPtr_line(test_string);

//   TEST_ASSERT_TRUE(gps__parse_coordinates_from_line());
//   TEST_ASSERT_FLOAT_WITHIN(0.0001, 37.330515f, parsed_coordinates.latitude);
//   TEST_ASSERT_FLOAT_WITHIN(0.0001, -121.905258f, parsed_coordinates.longitude);
// }

// void test_gps_parse_coordinates_from_valid_gpgga_line_4(void) {
//   char test_string[] = "$GPGGA,070342.605,3719.8367,N,12154.3256,W,1,07,1.10,24.6,M,-25.7,M,,*62";

//   line_buffer__remove_line_ExpectAndReturn(NULL, NULL, 0, true);
//   line_buffer__remove_line_IgnoreArg_buffer();
//   line_buffer__remove_line_IgnoreArg_line();
//   line_buffer__remove_line_IgnoreArg_line_max_size();
//   line_buffer__remove_line_ReturnThruPtr_line(test_string);

//   TEST_ASSERT_TRUE(gps__parse_coordinates_from_line());
//   TEST_ASSERT_FLOAT_WITHIN(0.0001, 37.330612f, parsed_coordinates.latitude);
//   TEST_ASSERT_FLOAT_WITHIN(0.0001, -121.905426f, parsed_coordinates.longitude);
// }

void test_gps_parse_coordinates_from_invalid_gpgga_lines(void) {
  char *test_strings[] = {"$GPGGA,235950.800,,,,,0,0,,,M,,M,,*48", "$GPGGA,235951.800,,,,,0,0,,,M,,M,,*49",
                          "$GPGGA,235952.800,,,,,0,0,,,M,,M,,*4A", "$GPGGA,235953.800,,,,,0,0,,,M,,M,,*4B"};

  for (int i = 0; i < sizeof(test_strings) / sizeof(test_strings[0]); i++) {

    line_buffer__remove_line_ExpectAndReturn(NULL, NULL, 0, true);
    line_buffer__remove_line_IgnoreArg_buffer();
    line_buffer__remove_line_IgnoreArg_line();
    line_buffer__remove_line_IgnoreArg_line_max_size();
    line_buffer__remove_line_ReturnThruPtr_line(test_strings[i]);

    TEST_ASSERT_FALSE(gps__parse_coordinates_from_line());
    TEST_ASSERT_FALSE(parsed_coordinates.valid);
    // turn_off_led_0_Expect();
    TEST_ASSERT_FALSE(gps_fix);
  }
}

// $GPGGA,065922.000,3719.8312,N,12154.3158,W,1,07,1.10,21.5,M,-25.7,M,,*60
// $GPGGA,065923.000,3719.8319,N,12154.3167,W,1,07,1.10,23.8,M,-25.7,M,,*69
// $GPGGA,065924.000,3719.8309,N,12154.3155,W,1,07,1.10,24.8,M,-25.7,M,,*69
// $GPGGA,070342.605,3719.8367,N,12154.3256,W,1,07,1.10,24.6,M,-25.7,M,,*62
// $GPGGA,235950.800,,,,,0,0,,,M,,M,,*48
// $GPGGA,235951.800,,,,,0,0,,,M,,M,,*49
// $GPGGA,235952.800,,,,,0,0,,,M,,M,,*4A
// $GPGGA,235953.800,,,,,0,0,,,M,,M,,*4B
