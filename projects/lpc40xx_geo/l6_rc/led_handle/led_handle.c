#include "led_handle.h"

// ***********************************************************************
// *                             Private                                 *
// ***********************************************************************

const static uint8_t led_delay = 40;
const static uint8_t short_led_delay = 20;

// ***********************************************************************
// *                             Public                                  *
// ***********************************************************************

void blink_all_leds(void) {
  gpio__reset(board_io__get_led0());
  gpio__reset(board_io__get_led1());
  gpio__reset(board_io__get_led2());
  gpio__reset(board_io__get_led3());
  vTaskDelay(short_led_delay);
  gpio__set(board_io__get_led0());
  gpio__set(board_io__get_led1());
  gpio__set(board_io__get_led2());
  gpio__set(board_io__get_led3());
  vTaskDelay(led_delay);
}

void turn_off_all_leds(void) {
  gpio__set(board_io__get_led0());
  gpio__set(board_io__get_led1());
  gpio__set(board_io__get_led2());
  gpio__set(board_io__get_led3());
}

void turn_off_led_0(void) { gpio__set(board_io__get_led0()); }

void turn_on_led_0(void) { gpio__reset(board_io__get_led0()); }

void turn_off_led_3(void) { gpio__set(board_io__get_led3()); }

void turn_on_led_3(void) { gpio__reset(board_io__get_led3()); }

void blink_led_0(void) {
  gpio__reset(board_io__get_led0());
  vTaskDelay(led_delay);
  gpio__set(board_io__get_led0());
  vTaskDelay(led_delay);
}

void blink_led_1(void) {
  gpio__reset(board_io__get_led1());
  vTaskDelay(led_delay);
  gpio__set(board_io__get_led1());
  vTaskDelay(led_delay);
}

void blink_led_2(void) {
  gpio__reset(board_io__get_led2());
  vTaskDelay(led_delay);
  gpio__set(board_io__get_led2());
  vTaskDelay(led_delay);
}

void blink_led_3(void) {
  gpio__reset(board_io__get_led3());
  vTaskDelay(led_delay);
  gpio__set(board_io__get_led3());
  vTaskDelay(led_delay);
}
