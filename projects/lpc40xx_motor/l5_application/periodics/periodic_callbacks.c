#include "periodic_callbacks.h"
#include "board_io.h"
#include "calibration.h"
#include "can_bus_initializer.h"
#include "gpio.h"
#include "led_handler.h"
#include "motor_controller.h"
#include "msg_bank_motor.h"
#include "rpm_sensor.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  // This method is invoked once when the periodic tasks are created
  my_can_init(can1);
  turn_off_onboard_leds();
  calibration_initalize();
  rpm_interrupt_init();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  if (callback_count % 2 == 0) {
    calcuate_speed_0_5Hz();
  }
  // Add your code here
}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  // calibration_runonce_drive();
  // calibration_runonce_steer();
  // Add your code here
}
void periodic_callbacks__100Hz(uint32_t callback_count) {
  if (callback_count % 5 == 0) {
    motor_can_handler__manage_mia_20hz();
    motor_can_handler__handle_all_incoming_messages_20hz();
    motor_control_function();
    can_handler__transmit_messages_20hz();
  }

  // Add your code here
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {

  // Add your code here
}