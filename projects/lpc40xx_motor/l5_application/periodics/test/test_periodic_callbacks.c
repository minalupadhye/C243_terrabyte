#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockcalibration.h"
#include "Mockcan_bus_initializer.h"
#include "Mockdelay.h"
#include "Mockgpio.h"
#include "Mockled_handler.h"
#include "Mockmotor_controller.h"
#include "Mockmsg_bank_motor.h"
#include "Mockpwm1.h"
#include "Mockrpm_sensor.h"

// Include the source we wish to test
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  my_can_init_Expect(can1);
  turn_off_onboard_leds_Expect();
  calibration_initalize_Expect();
  rpm_interrupt_init_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) {
  calcuate_speed_0_5Hz_Expect();
  periodic_callbacks__1Hz(0);
  periodic_callbacks__1Hz(1);
  calcuate_speed_0_5Hz_Expect();
  periodic_callbacks__1Hz(2);
  periodic_callbacks__1Hz(3);
  calcuate_speed_0_5Hz_Expect();
  periodic_callbacks__1Hz(4);
  periodic_callbacks__1Hz(5);
}

void test__periodic_callbacks__10Hz(void) {
  // // calibration_runonce_drive_Expect();
  // // calibration_runonce_steer_Expect();
  periodic_callbacks__10Hz(1);
}

void test__periodic_callbacks__100Hz(void) {
  motor_can_handler__manage_mia_20hz_Expect();
  motor_can_handler__handle_all_incoming_messages_20hz_Expect();
  motor_control_function_Expect();
  can_handler__transmit_messages_20hz_Expect();
  periodic_callbacks__100Hz(0);
  periodic_callbacks__100Hz(1);
  periodic_callbacks__100Hz(2);
  periodic_callbacks__100Hz(3);
  periodic_callbacks__100Hz(4);
  motor_can_handler__manage_mia_20hz_Expect();
  motor_can_handler__handle_all_incoming_messages_20hz_Expect();
  motor_control_function_Expect();
  can_handler__transmit_messages_20hz_Expect();
  periodic_callbacks__100Hz(5);
}