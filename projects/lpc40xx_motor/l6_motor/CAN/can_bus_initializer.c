#include "can_bus_initializer.h"
#include "print_enable.h"

void my_can_init(can__num_e CAN_number) {
  if (can__init(CAN_number, 500, 20, 20, NULL, NULL)) {
    can__bypass_filter_accept_all_msgs();
    can__reset_bus(CAN_number);
#if PRINT_BASED_DEBUG_CAN_INIT == 1
    printf("Initialized CAN bus.\n");
#endif

  } else {
#if PRINT_BASED_DEBUG_CAN_INIT == 1
    printf("CAN channel not initialized.\n");
#endif
  }
}
