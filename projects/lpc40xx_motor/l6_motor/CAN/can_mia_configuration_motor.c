#include "project.h"

/* Wait for 500ms */
const uint32_t dbc_mia_threshold_DRIVER_CONTROL = 500;

// Leave uninitialized if we wish to accept zero values as sane MIA replacement
const dbc_DRIVER_CONTROL_s dbc_mia_replacement_DRIVER_CONTROL = {};
