#include "dbc_to_sensor_driver_glue.h"

#include "can_bus.h"

bool dbc_send_can_message(void *argument_from_dbc_encode_and_send, uint32_t message_id, const uint8_t bytes[8],
                          uint8_t dlc) {
  can__msg_t msg = {};

  msg.frame_fields.data_len = dlc;
  msg.msg_id = message_id;
  memcpy(msg.data.bytes, bytes, 8);

  return can__tx(can1, &msg, 0);
}