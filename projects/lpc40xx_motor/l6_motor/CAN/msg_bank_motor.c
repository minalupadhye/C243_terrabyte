#include "msg_bank_motor.h"
#include "print_enable.h"

static dbc_DRIVER_CONTROL_s decoded_motor_cmd = {};

void motor_can_handler__manage_mia_20hz(void) {
  // We are in 10hz slot, increment MIA counter by 100ms, mia throshold = 500 => MIA actiated in 0.5s.
  const uint32_t mia_increment_value = 100;

  if (dbc_service_mia_DRIVER_CONTROL(&decoded_motor_cmd, mia_increment_value)) {
    turn_on_all_onboard_leds();
#if PRINT_BASED_DEBUG_MSG_BANK == 1
    printf("Motor: In Mia \n");
#endif
  }
}

void motor_can_handler__handle_all_incoming_messages_20hz(void) {
  can__msg_t rx_can_msg = {};

  while (can__rx(can1, &rx_can_msg, 0)) {
    // Construct "message header" that we need for the decode_*() API
    const dbc_message_header_t header = {
        .message_id = rx_can_msg.msg_id,
        .message_dlc = rx_can_msg.frame_fields.data_len,
    };

    // Even if incoming message is NOT motor cmd, our decode functions
    // will gracefully handle it because we provide valid "message header"

    // change according to received message
    motor_msg_bank__receive_msg(header, rx_can_msg);
  }
}

void motor_msg_bank__receive_msg(dbc_message_header_t header, can__msg_t can_msg) {
  if (dbc_decode_DRIVER_CONTROL(&decoded_motor_cmd, header, can_msg.data.bytes)) {
    turn_off_onboard_leds();
  }
#if PRINT_BASED_DEBUG_MSG_BANK == 1
  printf("Motor Message received\n");
  printf("Motor Speed %d \n", decoded_motor_cmd.DRIVER_CONTROL_speed);
  printf("Motor Steer %d \n", decoded_motor_cmd.DRIVER_CONTROL_steer);
#endif
}

// helper functions for testing Mia
void motor_handler_set_receive_msg_for_test(dbc_DRIVER_CONTROL_s test_decoded_motor_cmd) {
  decoded_motor_cmd = test_decoded_motor_cmd;
}

// function to get the latest copy of the received motor_cmd data
dbc_DRIVER_CONTROL_s get_motor_cmd(void) {
  dbc_DRIVER_CONTROL_s motor_cmd;
  motor_cmd = decoded_motor_cmd;
  return motor_cmd;
}

void can_handler__transmit_messages_20hz(void) {
  // printf("Entered Tx function\n");
  dbc_MOTOR_STATUS_s motor_status;

  motor_status.MOTOR_STATUS_wheel_speed = get_rpm_sensor_speed();
  if (dbc_encode_and_send_MOTOR_STATUS(NULL, &motor_status)) {
#if PRINT_BASED_DEBUG_MSG_BANK == 1
    float w_speed;
    w_speed = get_rpm_sensor_speed();
    printf("wheel_speed = w_speed \n");
#endif
  }
}