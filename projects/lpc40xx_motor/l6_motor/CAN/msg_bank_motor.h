#pragma once

#include <stdint.h>
#include <stdio.h>

#include "can_bus.h"
#include "dbc_to_sensor_driver_glue.h"
#include "led_handler.h"
#include "project.h"
#include "rpm_sensor.h"

void motor_can_handler__manage_mia_20hz(void);
void motor_can_handler__handle_all_incoming_messages_20hz(void);
void motor_msg_bank__receive_msg(dbc_message_header_t header, can__msg_t can_msg);

void can_handler__transmit_messages_20hz(void);

// helper functions for testing Mia - DO NOT SCHEDULE
void motor_handler_set_receive_msg_for_test(dbc_DRIVER_CONTROL_s test_decoded_motor_cmd);

// Shall be called by other functions to get the latest motor_cmd data. DO NOT SCHEDULE
dbc_DRIVER_CONTROL_s get_motor_cmd(void);
