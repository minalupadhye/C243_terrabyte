#include "calibration.h"

static gpio_s switch_01;
static gpio_s switch_02;
static gpio_s switch_03;
static gpio_s switch_04;

void calibration_initalize(void) {
  pwm1__init_single_edge(100);
  gpio__construct_with_function(GPIO__PORT_2, 0, GPIO__FUNCTION_1);
  gpio__construct_with_function(GPIO__PORT_2, 1, GPIO__FUNCTION_1);
  switch_01 = gpio__construct_as_input(GPIO__PORT_1, 19);
  switch_02 = gpio__construct_as_input(GPIO__PORT_1, 15);
  switch_03 = gpio__construct_as_input(GPIO__PORT_0, 30);
  switch_04 = gpio__construct_as_input(GPIO__PORT_0, 29);
  pwm1__set_duty_cycle(0, 15.0);
  delay__ms(3000);
}

void calibration_runonce_drive(void) {
  pwm1__set_duty_cycle(0, 15.0);

  if (gpio__get(switch_01)) {
    pwm1__set_duty_cycle(0, 20.0);
  } else if (gpio__get(switch_02)) {
    pwm1__set_duty_cycle(0, 10.0);
  }
}

void calibration_runonce_steer(void) {
  delay__us(1);
  pwm1__set_duty_cycle(1, 15.0);

  if (gpio__get(switch_03)) {
    // float right_angle = 15.00;
    // while (right_angle <= 20.00) {
    //   pwm1__set_duty_cycle(1, right_angle);
    //   right_angle += 1;
    // }
    pwm1__set_duty_cycle(1, 20.00);

  } else if (gpio__get(switch_04)) {
    // float left_angle = 15.00;
    // while (left_angle <= 10.00) {
    //   pwm1__set_duty_cycle(1, left_angle);
    //   left_angle -= 1;
    //   }
    pwm1__set_duty_cycle(1, 10.00);
  }
}