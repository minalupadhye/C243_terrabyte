#include "led_handler.h"

#include "FreeRTOS.h"
#include "task.h"

uint8_t check_switch_status(void) { return gpio__get(board_io__get_sw0()); }

void turn_off_onboard_leds(void) {
  gpio__set(board_io__get_led0());
  gpio__set(board_io__get_led1());
  gpio__set(board_io__get_led2());
  gpio__set(board_io__get_led3());
}

void turn_on_all_onboard_leds(void) {
  turn_on_led0();
  turn_on_led1();
  turn_on_led2();
  turn_on_led3();
}

void blink_all_onboard_leds(void) {
  turn_on_all_onboard_leds();

  vTaskDelay(50);

  turn_off_onboard_leds();
}

void turn_on_led0(void) { gpio__reset(board_io__get_led0()); }

void turn_on_led1(void) { gpio__reset(board_io__get_led1()); }

void turn_on_led2(void) { gpio__reset(board_io__get_led2()); }

void turn_on_led3(void) { gpio__reset(board_io__get_led3()); }

void turn_off_led0(void) { gpio__set(board_io__get_led0()); }

void turn_off_led1(void) { gpio__set(board_io__get_led1()); }

void turn_off_led2(void) { gpio__set(board_io__get_led2()); }

void turn_off_led3(void) { gpio__set(board_io__get_led3()); }