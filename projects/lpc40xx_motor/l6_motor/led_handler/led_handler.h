#pragma once

#include "board_io.h"

uint8_t check_switch_status(void);
void turn_off_onboard_leds(void);
void turn_on_all_onboard_leds(void);
void blink_all_onboard_leds(void);
void turn_on_led0(void);
void turn_on_led1(void);
void turn_on_led2(void);
void turn_on_led3(void);
void turn_off_led0(void);
void turn_off_led1(void);
void turn_off_led2(void);
void turn_off_led3(void);
