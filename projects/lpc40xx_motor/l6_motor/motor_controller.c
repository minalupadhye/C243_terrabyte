#include "motor_controller.h"

static void forward(int8_t);
static void brake(void);
static void reverse(void);

static bool inReverse = 0;

void motor_control_function(void) {
  int8_t speed_level;
  int8_t steer;
  dbc_DRIVER_CONTROL_s motor_cmd_data = {};
  motor_cmd_data = get_motor_cmd();

  speed_level = motor_cmd_data.DRIVER_CONTROL_speed;
  steer = motor_cmd_data.DRIVER_CONTROL_steer;

  helper_forward_reverse_break_control(speed_level);
  helper_steer_control(steer);
}

void helper_forward_reverse_break_control(int8_t f_speed_level) {
  pwm1__set_duty_cycle(0, 15.0);

  if (f_speed_level > 0) {
    forward(f_speed_level);
    inReverse = 0;

  } else if (f_speed_level == 0) {
    brake();

  } else if (f_speed_level < 0) {
    if (inReverse == 0) {
      brake();
      inReverse = 1;
    }

    reverse();
  }
}

void helper_steer_control(int8_t f_steer) {
  pwm1__set_duty_cycle(1, 15.0);

  if (f_steer == -2) {
    pwm1__set_duty_cycle(1, 10.0);
  } else if (f_steer == -1) {
    pwm1__set_duty_cycle(1, 12.5);
  } else if (f_steer == 0) {
    pwm1__set_duty_cycle(1, 15.0);
  } else if (f_steer == 1) {
    pwm1__set_duty_cycle(1, 17.5);
  } else if (f_steer == 2) {
    pwm1__set_duty_cycle(1, 20.0);
  }
}

static void forward(int8_t f_speed_level) {
  pwm1__set_duty_cycle(0, 15.0);

  float f_target_speed;
  float f_current_speed;
  float inital_pwm = 16.4;
  float dutycycle;

  f_target_speed = speedlevel_to_targetspeed(f_speed_level);
  f_current_speed = get_rpm_sensor_speed();
  dutycycle = p_Ctrlr(inital_pwm, f_target_speed, f_current_speed);
  pwm1__set_duty_cycle(0, dutycycle);

  turn_on_led0();
}

static void brake(void) {
  pwm1__set_duty_cycle(0, 10.0);
  delay__ms(3);
  pwm1__set_duty_cycle(0, 15.0);
  turn_on_led1();
}

static void reverse(void) {
  // pwm1__set_duty_cycle(0, 15.0);
  pwm1__set_duty_cycle(0, 13.5);
  turn_on_led2();
}

float speedlevel_to_targetspeed(int8_t speed_level) {
  float target_speed;
  switch (speed_level) {
  case 1:
    target_speed = 3; // in kmph
    break;

  case 2:
    target_speed = 6; // in kmph
    break;

  default:
    target_speed = 3; // in kmph
    break;
  }
  return target_speed;
}

float p_Ctrlr(float inital_pwm, float target_speed, float current_speed) {
  float pwm_dutycycle;
  float err;
  const float Kp = 0.05;
  const float pwm_min_forward = 16;
  const float pwm_max_forward = 20;

  err = target_speed - current_speed;
  pwm_dutycycle = inital_pwm;
  pwm_dutycycle = min(max(((err * Kp) + pwm_dutycycle), pwm_min_forward), pwm_max_forward);
  return pwm_dutycycle;
}