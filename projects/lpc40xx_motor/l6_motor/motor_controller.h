#pragma once

#include "pwm1.h"

#include "delay.h"
#include "led_handler.h"
#include "msg_bank_motor.h"
#include "rpm_sensor.h"

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

void motor_control_function(void);
void helper_forward_reverse_break_control(int8_t speed_level);
void helper_steer_control(int8_t steer);
float speedlevel_to_targetspeed(int8_t speed_level);
float p_Ctrlr(float inital_pwm, float target_speed, float current_speed);
