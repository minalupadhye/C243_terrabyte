#include "rpm_sensor.h"
#include "print_enable.h"

static uint8_t rotations_cnt;
static float speed;
static bool enable_rpm_up_cnt_flg = 1;
static uint64_t EF_up_time;                 // in us
static uint64_t ER_up_time;                 // in us
static uint64_t const rpm_enable_thd = 700; // in us

void rpm_interrupt_init(void) {
  rotations_cnt = 0;

  gpio__construct_as_input(GPIO__PORT_0, 6); /* Right Sensor Echo */

  LPC_GPIOINT->IO0IntEnF |= (1 << 6);
  LPC_GPIOINT->IO0IntEnR |= (1 << 6);

  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, rpm_interrupt_handler, "RPM interupt");
  NVIC_EnableIRQ(GPIO_IRQn);
}

void rpm_interrupt_handler(void) {
  if ((LPC_GPIOINT->IO0IntStatF & (1 << 6)) && enable_rpm_up_cnt_flg) {
    rotations_cnt += 1;
    EF_up_time = sys_time__get_uptime_us();
    enable_rpm_up_cnt_flg = 0;
  }

  if (LPC_GPIOINT->IO0IntStatR & (1 << 6)) {
    ER_up_time = sys_time__get_uptime_us();
    if ((ER_up_time - EF_up_time) > rpm_enable_thd) {
      enable_rpm_up_cnt_flg = 1;
    }
  }

  LPC_GPIOINT->IO0IntClr = (1 << 6);
}

void calcuate_speed_0_5Hz(void) {
  const float wheel_circumference = 34.54; // in cms
  const float gear_ratio = 2.73;

#if PRINT_BASED_DEBUG_RPM_SENSOR == 1
  printf("No. of rotations is %d \n", rotations_cnt);
#endif
  // dividing rotations_cnt by 2 since we are getting 2 pulses in a rpm.
  rotations_cnt = rotations_cnt / 2;
  // has a divide by 2 since the count is for every 2s
  speed = 3600 * rotations_cnt * wheel_circumference / (1000 * 100 * gear_ratio * 2);
  rotations_cnt = 0;
#if PRINT_BASED_DEBUG_RPM_SENSOR == 1
  printf("Speed in kmph %f \n", speed);
#endif
}

float get_rpm_sensor_speed(void) {
  float return_speed;
  return_speed = speed;
  return return_speed;
}