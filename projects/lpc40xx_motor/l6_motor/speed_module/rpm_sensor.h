#pragma once

#include <stdint.h>
#include <stdio.h>

#include "gpio.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "sys_time.h"

void rpm_interrupt_init(void);
void rpm_interrupt_handler(void);
void calcuate_speed_0_5Hz(void);
float get_rpm_sensor_speed(void);