#include "unity.h"

#include "Mockcan_bus.h"
#include "can_bus_initializer.h"

void test_my_can_init(void) {
  can__init_ExpectAndReturn(can1, 500, 20, 20, NULL, NULL, true);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(can1);

  my_can_init(can1);
}
