#include "unity.h"

#include "Mockdelay.h"
#include "Mockled_handler.h"
#include "Mockmsg_bank_motor.h"
#include "Mockpwm1.h"
#include "Mockrpm_sensor.h"

#include "motor_controller.h"

// --- forward backward and reverse function test ---
void test_helper_forward_control(void) {
  pwm1__set_duty_cycle_Expect(0, 15.0);
  pwm1__set_duty_cycle_Expect(0, 15.0);
  get_rpm_sensor_speed_ExpectAndReturn(3);
  pwm1__set_duty_cycle_Expect(0, 16.4);
  turn_on_led0_Expect();
  helper_forward_reverse_break_control(1);
}

void test_helper_break_control(void) {
  pwm1__set_duty_cycle_Expect(0, 15.0);
  pwm1__set_duty_cycle_Expect(0, 10.0);
  delay__ms_Expect(3);
  pwm1__set_duty_cycle_Expect(0, 15.0);
  turn_on_led1_Expect();
  helper_forward_reverse_break_control(0);
}

void test_helper_reverse_control(void) {
  pwm1__set_duty_cycle_Expect(0, 15.0);
  pwm1__set_duty_cycle_Expect(0, 10.0);
  delay__ms_Expect(3);
  pwm1__set_duty_cycle_Expect(0, 15.0);
  turn_on_led1_Expect();
  pwm1__set_duty_cycle_Expect(0, 13.5);
  turn_on_led2_Expect();
  helper_forward_reverse_break_control(-1);
}

// --------  steer function test --------------
void test_helper_steer_right_hard_control(void) {
  pwm1__set_duty_cycle_Expect(1, 15.0);
  pwm1__set_duty_cycle_Expect(1, 20.0);
  helper_steer_control(2);
}

void test_helper_steer_right_soft_control(void) {
  pwm1__set_duty_cycle_Expect(1, 15.0);
  pwm1__set_duty_cycle_Expect(1, 17.5);
  helper_steer_control(1);
}

void test_helper_steer_nosteer_control(void) {
  pwm1__set_duty_cycle_Expect(1, 15.0);
  pwm1__set_duty_cycle_Expect(1, 15.0);
  helper_steer_control(0);
}

void test_helper_steer_left_hard_control(void) {
  pwm1__set_duty_cycle_Expect(1, 15.0);
  pwm1__set_duty_cycle_Expect(1, 10.0);
  helper_steer_control(-2);
}

void test_helper_steer_left_soft_control(void) {
  pwm1__set_duty_cycle_Expect(1, 15.0);
  pwm1__set_duty_cycle_Expect(1, 12.5);
  helper_steer_control(-1);
}

// Test speed level to target speed converter
void test_speed_level_to_target_speed_converter(void) {
  TEST_ASSERT_EQUAL(3, speedlevel_to_targetspeed(1));
  TEST_ASSERT_EQUAL(6, speedlevel_to_targetspeed(2));
  TEST_ASSERT_EQUAL(3, speedlevel_to_targetspeed(5));
}

// Test P controller
void test_p_Ctrlr_max_value(void) { TEST_ASSERT_EQUAL(20, p_Ctrlr(19, 220, 0)); }

void test_p_Ctrlr_min_value(void) { TEST_ASSERT_EQUAL(16, p_Ctrlr(16.4, 3, 51)); }

void test_p_Ctrlr_operation(void) {
  TEST_ASSERT_EQUAL(16.75, p_Ctrlr(16.4, 10, 3));
  TEST_ASSERT_EQUAL(19.45, p_Ctrlr(16.4, 64, 3));
  TEST_ASSERT_EQUAL(16.55, p_Ctrlr(16.4, 6, 3));
  TEST_ASSERT_EQUAL(16.25, p_Ctrlr(16.4, 3, 6));
  TEST_ASSERT_EQUAL(16, p_Ctrlr(16.4, 6, 23));
  TEST_ASSERT_EQUAL(16.05, p_Ctrlr(16.4, 3, 10));
}