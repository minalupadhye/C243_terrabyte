#include "unity.h"

#include "Mockcan_bus.h"
#include "Mockled_handler.h"
#include "Mockrpm_sensor.h"
#include "can_mia_configuration_motor.c"
#include "dbc_to_sensor_driver_glue.h"
#include "msg_bank_motor.h"
#include "project.h"

void setUp(void) {}

void tearDown(void) {}

void test_motor_can_handler__manage_mia_20hz_NoMia(void) {
  dbc_DRIVER_CONTROL_s test_decoded_motor_cmd = {};
  test_decoded_motor_cmd.mia_info.mia_counter = 0;
  motor_handler_set_receive_msg_for_test(test_decoded_motor_cmd);

  motor_can_handler__manage_mia_20hz();
}

void test_motor_can_handler__manage_mia_20hz_Mia(void) {
  dbc_DRIVER_CONTROL_s test_decoded_motor_cmd = {};
  test_decoded_motor_cmd.mia_info.mia_counter = 400;
  motor_handler_set_receive_msg_for_test(test_decoded_motor_cmd);

  turn_on_all_onboard_leds_Expect();

  motor_can_handler__manage_mia_20hz();
}

void test_motor_can_handler__handle_all_incoming_messages_20hz_msg_Received_successfully(void) {
  can__msg_t test_rx_can_msg = {};

  test_rx_can_msg.msg_id = 300;
  test_rx_can_msg.frame_fields.data_len = 2;
  test_rx_can_msg.data.bytes[0] = 1;

  can__rx_ExpectAndReturn(can1, &test_rx_can_msg, 0, true);
  can__rx_IgnoreArg_can_message_ptr();
  can__rx_ReturnThruPtr_can_message_ptr(&test_rx_can_msg);

  can__rx_ExpectAndReturn(can1, NULL, 0, false);
  can__rx_IgnoreArg_can_message_ptr();

  turn_off_onboard_leds_Expect();

  motor_can_handler__handle_all_incoming_messages_20hz();
}

void test_motor_can_handler__handle_all_incoming_messages_20hz_msg_from_Unknown_Sender(void) {
  can__msg_t test_rx_can_msg = {};

  test_rx_can_msg.msg_id = 250;
  test_rx_can_msg.frame_fields.data_len = 1;
  test_rx_can_msg.data.bytes[0] = 5;

  can__rx_ExpectAndReturn(can1, &test_rx_can_msg, 0, true);
  can__rx_IgnoreArg_can_message_ptr();
  can__rx_ReturnThruPtr_can_message_ptr(&test_rx_can_msg);

  can__rx_ExpectAndReturn(can1, NULL, 0, false);
  can__rx_IgnoreArg_can_message_ptr();

  motor_can_handler__handle_all_incoming_messages_20hz();
}

void test_get_motor_cmd(void) {
  dbc_DRIVER_CONTROL_s test_decoded_motor_cmd = {};
  dbc_DRIVER_CONTROL_s check_decoded_motor_cmd = {};
  test_decoded_motor_cmd.mia_info.mia_counter = 0;
  test_decoded_motor_cmd.DRIVER_CONTROL_speed = -9;
  test_decoded_motor_cmd.DRIVER_CONTROL_steer = -2;

  motor_handler_set_receive_msg_for_test(test_decoded_motor_cmd);
  check_decoded_motor_cmd = get_motor_cmd();

  TEST_ASSERT_EQUAL(test_decoded_motor_cmd.mia_info.mia_counter, check_decoded_motor_cmd.mia_info.mia_counter);
  TEST_ASSERT_EQUAL(test_decoded_motor_cmd.DRIVER_CONTROL_speed, check_decoded_motor_cmd.DRIVER_CONTROL_speed);
  TEST_ASSERT_EQUAL(test_decoded_motor_cmd.DRIVER_CONTROL_steer, check_decoded_motor_cmd.DRIVER_CONTROL_steer);
}