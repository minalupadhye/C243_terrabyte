#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "periodic_scheduler.h"

int main(void) {

  /*
    Bluetooth pins: (UART 2; 0.10 Tx, 0.11 Rx)

    Ultra Sonic Sensor:
      Front: (GPIO; 0.15 Trig, 0.16 Echo)
      Rear: (GPIO; 0.26 Trig, 0.25 Echo)
      Left:  (GPIO; 0.6 Trig, 0.7 Echo)
      Right: (GPIO; 0.8 Trig, 0.9 Echo)
  */

  periodic_scheduler__initialize();
  puts("Starting RTOS");
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails
  return 0;
}
