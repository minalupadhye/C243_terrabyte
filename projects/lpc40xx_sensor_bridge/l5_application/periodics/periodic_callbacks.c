#include "periodic_callbacks.h"

#include "bridge_bluetooth.h"
#include "can_bus_initializer.h"
#include "can_handler.h"
#include "sensor_handler.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  // This method is invoked once when the periodic tasks are created
  my_can_init(can1);
  sensor_init();
  bluetooth_init();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  // Add your code here
  bluetooth_send_string("test\n");
}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  // Add your code here
}
void periodic_callbacks__100Hz(uint32_t callback_count) {
  // Add your code here
  if ((callback_count % 5) == 0) {
    can_handler__transmit_messages_10hz();
    bluetooth_receive();
    // can_handler__receive_messages_10hz();
  }
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {
  // Add your code here
}