#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockbridge_bluetooth.h"
#include "Mockcan_bus_initializer.h"
#include "Mockcan_handler.h"
#include "Mocksensor_handler.h"

// Include the source we wish to test
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  my_can_init_Expect(can1);
  bluetooth_init_Expect();
  sensor_init_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__10Hz(void) {
  // can_handler__transmit_messages_10hz_Expect();
  // bluetooth_send_Expect('a');
  // bluetooth_receive_Expect();
  // periodic_callbacks__10Hz(1);
}

void test__periodic_callbacks_100Hz(void) {
  for (int i = 0; i < 10; i++) {
    if ((i % 5) == 0) {
      can_handler__transmit_messages_10hz_Expect();
      bluetooth_receive_Expect();
      // bluetooth_send_Expect('a');
    }
    periodic_callbacks__100Hz(i);
  }
}