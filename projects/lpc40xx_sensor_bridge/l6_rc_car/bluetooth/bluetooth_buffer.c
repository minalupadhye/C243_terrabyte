#include "bluetooth_buffer.h"

#include <string.h>

void ble_buffer__init(ble_buffer_s *buffer, void *memory, size_t size) {
  if (buffer != NULL && memory != NULL) {
    buffer->write_index = 0;
    buffer->max_size = size;
    buffer->memory = memory;
  }
}

bool ble_buffer__add_byte(ble_buffer_s *buffer, char byte) {
  if (buffer != NULL) {
    if (buffer->write_index < buffer->max_size) {
      ((char *)(buffer->memory))[buffer->write_index++] = byte;
      return true;
    }
  }
  return false;
}

static size_t find_newline(const ble_buffer_s *buffer) {
  for (size_t i = 0; i < buffer->write_index; ++i) {
    if (((char *)buffer->memory)[i] == '\n') {
      return i; // Newline found, return its position
    }
  }
  return buffer->write_index; // No newline found, return the write index
}

static void copy_line_to_buffer(ble_buffer_s *buffer, char *line, size_t newline_position, size_t line_max_size) {
  size_t copy_length = newline_position < line_max_size ? newline_position : line_max_size - 1;
  memcpy(line, buffer->memory, copy_length);
  line[copy_length] = '\0'; // Null-terminate the line
}

static void shift_buffer_contents(ble_buffer_s *buffer, size_t shift_amount) {
  memmove(buffer->memory, (char *)buffer->memory + shift_amount, buffer->write_index - shift_amount);
  buffer->write_index -= shift_amount;
}

bool ble_buffer__remove_line(ble_buffer_s *buffer, char *line, size_t line_max_size) {
  if (buffer == NULL || line == NULL || line_max_size == 0) {
    return false; // Invalid parameters
  }

  size_t newline_pos = find_newline(buffer);
  bool found_newline = (newline_pos < buffer->write_index);

  // Handle the corner case where the buffer is full but there's no newline
  bool buffer_full_no_newline = (buffer->write_index == buffer->max_size && !found_newline);

  // If a line was found (or corner case), copy it to the provided buffer
  if (found_newline || buffer_full_no_newline) {
    size_t line_length = found_newline ? newline_pos : buffer->write_index; // Exclude newline if present
    copy_line_to_buffer(buffer, line, line_length, line_max_size);
    shift_buffer_contents(buffer, newline_pos + 1); // +1 to skip the newline character
    return true;
  }

  return false; // No complete line found
}
