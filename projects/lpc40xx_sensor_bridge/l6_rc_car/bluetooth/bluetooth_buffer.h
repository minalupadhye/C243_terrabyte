#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

typedef struct {
  void *memory;
  size_t max_size;
  size_t write_index;
} ble_buffer_s;

void ble_buffer__init(ble_buffer_s *buffer, void *memory, size_t size);

bool ble_buffer__add_byte(ble_buffer_s *buffer, char byte);

bool ble_buffer__remove_line(ble_buffer_s *buffer, char *line, size_t line_max_size);