#include "bridge_bluetooth.h"

#include "gpio.h"
#include "uart.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

ble_buffer_s ble_buffer;
char ble_memory[50] = {0};
float parsed_latitude = 0;
float parsed_longitude = 0;
bool start_stop_bit = 0;

void bluetooth_init(void) {
  gpio__construct_with_function(GPIO__PORT_0, 10, GPIO__FUNCTION_1); /* Tx */
  gpio__construct_with_function(GPIO__PORT_0, 11, GPIO__FUNCTION_1); /* Rx */

  QueueHandle_t queue_uart_tx = xQueueCreate(100, sizeof(char));
  QueueHandle_t queue_uart_rx = xQueueCreate(100, sizeof(char));

  uart__init(UART__2, 96 * 1000 * 1000, 9600);
  uart__enable_queues(UART__2, queue_uart_rx, queue_uart_tx);

  ble_buffer__init(&ble_buffer, ble_memory, sizeof(ble_memory));
}

void bluetooth_send(char byte_to_send) { uart__put(UART__2, byte_to_send, 0); }

void bluetooth_send_string(char *string_to_send) {
  for (int i = 0; i < strlen(string_to_send); i++) {
    bluetooth_send(string_to_send[i]);
  }
}

void bluetooth_receive(void) {
  char received = 0;
  while (uart__get(UART__2, &received, 0)) {
    ble_buffer__add_byte(&ble_buffer, received);
  }

  bluetooth_parse_data();
}

void bluetooth_parse_data(void) {
  char ble_msg[128] = {0};
  if (ble_buffer__remove_line(&ble_buffer, ble_msg, sizeof(ble_msg))) {
    printf("ble data: %s\n", ble_msg);
    if (strcmp(ble_msg, "stop") == 0) {
      // printf("Car will stop\n");
      start_stop_bit = 0;
    } else if (strcmp(ble_msg, "start") == 0) {
      // printf("Car will start\n");
      start_stop_bit = 1;
    } else if (strcmp(ble_msg, "") == 0) {
      printf("Invalid cmd\n");
    } else {
      printf("got coordinates\n");

      char *latitude = strtok(ble_msg, ",");
      char *longitude = strtok(NULL, ",");

      if (latitude && longitude) {
        // Convert and adjust coordinates
        parsed_latitude = atof(latitude);
        parsed_longitude = atof(longitude);
      }

      printf("Parsed Latitude: %f\tLongitude: %f\n", parsed_latitude, parsed_longitude);
    }
  }
}

dbc_GPS_DESTINATION_LOCATION_s get_scaled_destination_coordinates(void) {
  dbc_GPS_DESTINATION_LOCATION_s destination_coordinates;
  destination_coordinates.GPS_DEST_LATITUDE_SCALED_1000000 = parsed_latitude * 1000 * 1000;
  destination_coordinates.GPS_DEST_LONGITUDE_SCALED_1000000 = parsed_longitude * 1000 * 1000;

  return destination_coordinates;
}

dbc_APP_COMMAND_s get_app_cmd(void) {
  dbc_APP_COMMAND_s app_cmd;
  app_cmd.BRIDGE_START_STOP = start_stop_bit;

  return app_cmd;
}