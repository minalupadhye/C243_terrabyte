#pragma once

#include "bluetooth_buffer.h"
#include "project.h"

void bluetooth_init(void);

void bluetooth_send(char val);

void bluetooth_send_string(char *string_to_send);

void bluetooth_receive(void);

void bluetooth_parse_data(void);

dbc_GPS_DESTINATION_LOCATION_s get_scaled_destination_coordinates(void);

dbc_APP_COMMAND_s get_app_cmd(void);