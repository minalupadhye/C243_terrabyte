#include "can_bus_initializer.h"

#include <stdlib.h>

void my_can_init(can__num_e CAN_number) {
  if (can__init(CAN_number, 500, 20, 20, NULL, NULL)) {
    can__bypass_filter_accept_all_msgs();
    can__reset_bus(CAN_number);
  }
}