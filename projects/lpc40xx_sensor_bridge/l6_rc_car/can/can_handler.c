#include <stdio.h>

#include "can_handler.h"

#include "project.h"

#include "bridge_bluetooth.h"
#include "can_bus.h"
#include "sensor_handler.h"

void can_handler__transmit_messages_10hz(void) {
  set_sensor_values();
  const dbc_SENSOR_DATA_s sensor_struct = get_sensor_values();
  const dbc_APP_COMMAND_s app_cmd = get_app_cmd();

  dbc_GPS_DESTINATION_LOCATION_s gps_data = get_scaled_destination_coordinates();
  if (gps_data.GPS_DEST_LATITUDE_SCALED_1000000 == 0 && gps_data.GPS_DEST_LONGITUDE_SCALED_1000000 == 0) {
    gps_data.GPS_DEST_LATITUDE_SCALED_1000000 = 37329704;
    gps_data.GPS_DEST_LONGITUDE_SCALED_1000000 = -121905202;
  }

  if (dbc_encode_and_send_APP_COMMAND(NULL, &app_cmd)) {
    // printf("Sent start/stop bit\n");
  } else {
    // printf("could not transmit app cmd\n");
  }

  if (dbc_encode_and_send_SENSOR_DATA(NULL, &sensor_struct)) {
    // printf("transmitted sensor struct\n");
    // printf("front sensor data: %u\n", sensor_struct.SENSOR_DATA_front);
    // printf("rear sensor data: %u\n", sensor_struct.SENSOR_DATA_rear);
    // printf("right sensor data: %u\n", sensor_struct.SENSOR_DATA_right);
    // printf("left sensor data: %u\n\n", sensor_struct.SENSOR_DATA_left);
  } else {
    // printf("could not transmit sensor data\n");
  }

  if (dbc_encode_and_send_GPS_DESTINATION_LOCATION(NULL, &gps_data)) {
    // printf("latitude: %li\n", gps_data.GPS_DEST_LATITUDE_SCALED_1000000);
    // printf("longitude: %li\n\n", gps_data.GPS_DEST_LONGITUDE_SCALED_1000000);
  } else {
    // printf("could not transmit gps data\n");
  }
}

void can_handler__receive_messages_10hz(void) {}
