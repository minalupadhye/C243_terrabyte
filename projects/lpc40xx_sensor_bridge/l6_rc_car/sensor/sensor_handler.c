#include "sensor_handler.h"

#include <stdio.h>

#include "sensor_interrupt.h"

static dbc_SENSOR_DATA_s sensor_data = {};

void sensor_init(void) { sensor_interrupt_init(); }

void set_sensor_values(void) {
  set_sensor_sr04(get_trig_struct(sensor_left));
  set_sensor_sr04(get_trig_struct(sensor_right));
  set_sensor_sr04(get_trig_struct(sensor_front));
  set_sensor_sr04(get_trig_struct(sensor_rear));

  sensor_data.SENSOR_DATA_left = MIN_OF(read_sensor_sr04(sensor_left), 250);
  sensor_data.SENSOR_DATA_right = MIN_OF(read_sensor_sr04(sensor_right), 250);
  sensor_data.SENSOR_DATA_front = MIN_OF(read_sensor_sr04(sensor_front), 250);
  sensor_data.SENSOR_DATA_rear = MIN_OF(read_sensor_sr04(sensor_rear), 250);

#if 0
  printf("front sensor: %u\n", sensor_data.SENSOR_DATA_front);
  printf("rear sensor: %u\n", sensor_data.SENSOR_DATA_rear);
  printf("right sensor: %u\n", sensor_data.SENSOR_DATA_right);
  printf("left sensor: %u\n\n", sensor_data.SENSOR_DATA_left);

#endif
}

dbc_SENSOR_DATA_s get_sensor_values(void) { return sensor_data; }