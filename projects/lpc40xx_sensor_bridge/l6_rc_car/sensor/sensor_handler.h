#pragma once

#include "project.h"

void sensor_init(void);
void set_sensor_values(void);
dbc_SENSOR_DATA_s get_sensor_values(void);