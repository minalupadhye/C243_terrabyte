#include "sensor_interrupt.h"

#include "delay.h"
#include "sys_time.h"

#include <stdio.h>

uint64_t left_up_time = 0;
uint64_t right_up_time = 0;
uint64_t front_up_time = 0;
uint64_t rear_up_time = 0;
gpio_s left_trig_pin, right_trig_pin, front_trig_pin, rear_trig_pin;

/* Private Function*/

static uint8_t get_pin_that_generated_interrupt(void) {
  for (uint8_t pin_num = 0; pin_num < 32; pin_num++) {
    if (LPC_GPIOINT->IO0IntStatF & (1 << pin_num)) {
      return pin_num;
    }

    if (LPC_GPIOINT->IO0IntStatR & (1 << pin_num)) {
      return pin_num;
    }
  }
}

/* Public Functions*/

void sensor_interrupt_init(void) {
  /* Setup Trig/Echo pins */
  rear_trig_pin = gpio__construct_as_output(GPIO__PORT_0, 15); /* Front Sensor Trig */
  gpio__construct_as_input(GPIO__PORT_0, 16);                  /* Front Sensor Echo */

  front_trig_pin = gpio__construct_as_output(GPIO__PORT_0, 26); /* Rear Sensor Trig */
  gpio__construct_as_input(GPIO__PORT_0, 25);                   /* Rear Sensor Echo */

  left_trig_pin = gpio__construct_as_output(GPIO__PORT_0, 6); /* Left Sensor Trig */
  gpio__construct_as_input(GPIO__PORT_0, 7);                  /* Left Sensor Echo */

  right_trig_pin = gpio__construct_as_output(GPIO__PORT_0, 8); /* Right Sensor Trig */
  gpio__construct_as_input(GPIO__PORT_0, 9);                   /* Right Sensor Echo */

  /* Setup interupts */
  LPC_GPIOINT->IO0IntEnR |= (1 << 25);
  LPC_GPIOINT->IO0IntEnF |= (1 << 25);

  LPC_GPIOINT->IO0IntEnR |= (1 << 16);
  LPC_GPIOINT->IO0IntEnF |= (1 << 16);

  LPC_GPIOINT->IO0IntEnR |= (1 << 7);
  LPC_GPIOINT->IO0IntEnF |= (1 << 7);

  LPC_GPIOINT->IO0IntEnR |= (1 << 9);
  LPC_GPIOINT->IO0IntEnF |= (1 << 9);

  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, sensor_interrupt_handler, "sensor interupt");
  NVIC_EnableIRQ(GPIO_IRQn);

  sys_time__init(96000000);
}

void set_sensor_sr04(gpio_s pin) {
  gpio__set(pin);
  delay__us(10);
  gpio__reset(pin);
}

uint64_t read_sensor_sr04(enum pin_index index) {
  uint64_t width = 0;
  uint64_t sensor_value = 0;

  switch (index) {
  case sensor_left:
    width = left_up_time;
    break;

  case sensor_right:
    width = right_up_time;
    break;

  case sensor_front:
    width = front_up_time;
    break;

  case sensor_rear:
    width = rear_up_time;
    break;

  default:
    break;
  }

  sensor_value = (width * 0.017);
  return sensor_value;
}

gpio_s get_trig_struct(int index) {
  gpio_s pin;

  switch (index) {
  case sensor_left:
    pin = left_trig_pin;
    break;

  case sensor_right:
    pin = right_trig_pin;
    break;

  case sensor_front:
    pin = front_trig_pin;
    break;

  case sensor_rear:
    pin = rear_trig_pin;
    break;

  default:
    break;
  }

  return pin;
}

void sensor_interrupt_handler(void) {
  uint8_t pin_num = get_pin_that_generated_interrupt();
  uint64_t *sensor = 0;

  switch (pin_num) {
  case 7:
    sensor = &right_up_time;
    break;

  case 9:
    sensor = &left_up_time;
    break;

  case 16:
    sensor = &front_up_time;
    break;

  case 25:
    sensor = &rear_up_time;
    break;

  default:
    break;
  }

  if (LPC_GPIOINT->IO0IntStatR & (1 << pin_num)) {
    *sensor = sys_time__get_uptime_us();
  }

  if (LPC_GPIOINT->IO0IntStatF & (1 << pin_num)) {
    *sensor = sys_time__get_uptime_us() - *sensor;
  }

  LPC_GPIOINT->IO0IntClr = (1 << pin_num);
}