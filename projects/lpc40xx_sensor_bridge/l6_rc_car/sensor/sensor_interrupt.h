#pragma once

#include <stdint.h>

#include "gpio.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"

enum pin_index { sensor_left = 1, sensor_right, sensor_front, sensor_rear };

void sensor_interrupt_init(void);

void set_sensor_sr04(gpio_s pin);

uint64_t read_sensor_sr04(enum pin_index index);

void sensor_interrupt_handler(void);

gpio_s get_trig_struct(int index);
