#include "unity.h"

#include "can_bus_initializer.h"

#include "Mockcan_bus.h"

void test__can_bus_initialization(void) {
  can__init_ExpectAndReturn(can1, 500, 20, 20, NULL, NULL, 1);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_Expect(can1);

  my_can_init(can1);
}