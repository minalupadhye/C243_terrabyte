#include "unity.h"

#include "Mockbridge_bluetooth.h"
#include "Mockcan_bus.h"
#include "Mocksensor_handler.h"

#include "can_handler.h"
#include "dbc_to_sensor_driver_glue.h"

static dbc_SENSOR_DATA_s sensor_stub(void) {
  dbc_SENSOR_DATA_s sensor = {};
  return sensor;
}

static dbc_DRIVER_CONTROL_s driver_stub(void) {
  dbc_DRIVER_CONTROL_s driver = {};
  return driver;
}

void test__can_handler__transmit_messages_10hz(void) {
  // set_sensor_values_Expect();
  // //   get_sensor_values_Stub(sensor_stub());
  // get_sensor_values_ExpectAndReturn(sensor_stub());

  // can__msg_t test_tx_can_msg = {};

  // test_tx_can_msg.msg_id = 200;
  // test_tx_can_msg.frame_fields.data_len = 2;
  // test_tx_can_msg.data.bytes[0] = 1;

  // can__tx_ExpectAndReturn(can1, &test_tx_can_msg, 0, true);
  // can__tx_IgnoreArg_can_message_ptr();
  // can_handler__transmit_messages_10hz();
}